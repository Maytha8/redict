Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Drew DeVault <sir@cmpwn.com>
Upstream-Name: redict
Source: https://codeberg.org/redict/redict
License: LGPL-3.0-only
Files-Excluded:
 deps/hiredict
 deps/jemalloc
Comment: Listed vendored libraries are replaced with Debian counterparts.

Files: *
Copyright:
 2006-2014 Salvatore Sanfilippo <antirez@gmail.com>
 2024 Redict contributors
License: LGPL-3.0-only and BSD-3-Clause

Files:
 src/rio.*
 src/t_zset.c
 src/ziplist.h
 src/intset.*
 src/redict-check-aof.c
Copyright:
 2024 Redict contributors
 2009-2012 Pieter Noordhuis <pcnoordhuis@gmail.com>
 2009-2012 Salvatore Sanfilippo <antirez@gmail.com>
License: LGPL-3.0-only and BSD-3-Clause

Files: src/redictassert.c
Copyright:
 2024 Redict contributors
 2021 Andy Pan <panjf2000@gmail.com>
 2021 Redis Labs
License: LGPL-3.0-only and BSD-3-Clause

Files:
 src/lzf.h
 src/lzfP.h
 src/lzf_d.c
 src/lzf_c.c
Copyright:
 2000-2007 Marc Alexander Lehmann <schmorp@schmorp.de>
 2009-2012 Salvatore Sanfilippo <antirez@gmail.com>
 2024 Redict contributors
License: LGPL-3.0-only and BSD-2-Clause

Files: src/setproctitle.c
Copyright:
 2010 William Ahern
 2013 Salvatore Sanfilippo
 2013 Stam He
 2024 Redict contributors
License: LGPL-3.0-only and BSD-3-Clause

Files: src/ae_evport.c
Copyright:
 2012 Joyent, Inc.
 2024 Redict contributors
License: LGPL-3.0-only and BSD-3-Clause

Files: src/ae_kqueue.c
Copyright:
 2009 Harish Mallipeddi <harish.mallipeddi@gmail.com>
 2024 Redict contributors
License: LGPL-3.0-only and BSD-3-Clause

Files: src/pqsort.*
Copyright:
 1992-1993 The Regents of the University of California
 2024 Redict contributors
License: LGPL-3.0-only and BSD-3-Clause

Files: src/sds.*
Copyright:
 2024 Redict contributors
 2006-2015 Salvatore Sanfilippo <antirez@gmail.com>
 2015 Oran Agra
 2015 Redis Labs
License: LGPL-3.0-or-later and BSD-3-Clause

Files: deps/lua/*
Copyright: 1994-2012 Lua.org, PUC-Ri
License: Expat

Files: deps/fpconv/*
Copyright:
 2009 Florian Loitsch <florian.loitsch@inria.fr>
 2013-2019 night-shift <as.smljk@gmail.com>
 2021 Redis Labs
License: Boost-1.0

Files: deps/linenoise/*
Copyright:
 2010-2013 Pieter Noordhuis <pcnoordhuis@gmail.com>
 2010-2016 Salvatore Sanfilippo <antirez@gmail.com>
License: BSD-2-Clause

Files: debian/*
Copyright:
 2009—2024 Chris Lamb <lamby@debian.org>
 2024 Maytham Alsudany <maytha8thedev@gmail.com>
License: LGPL-3.0-only and BSD-3-Clause
Comment: Debian packaging is licensed under the same terms as upstream

License: LGPL-3.0-only
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, version 3 only.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 3 can be found in "/usr/share/common-licenses/LGPL-3", as well as the
 GNU General Public License version 3, which can be found in
 "/usr/share/common-licenses/GPL-3".

License: LGPL-3.0-or-later
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 3 can be found in "/usr/share/common-licenses/LGPL-3", as well as the
 GNU General Public License version 3, which can be found in
 "/usr/share/common-licenses/GPL-3".

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: Boost-1.0
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
