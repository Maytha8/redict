# Copyright (C) 2014 Salvatore Sanfilippo
# SPDX-FileCopyrightText: 2024 Redict Contributors
# SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
#
# SPDX-License-Identifier: BSD-3-Clause
# SPDX-License-Identifier: LGPL-3.0-only

require 'rubygems'
require 'redis'
require 'digest/sha1'

r = Redis.new
r.del('hll')
i = 0
while true do
    100.times {
        elements = []
        1000.times {
            ele = Digest::SHA1.hexdigest(i.to_s)
            elements << ele
            i += 1
        }
        r.pfadd('hll',elements)
    }
    approx = r.pfcount('hll')
    abs_err = (approx-i).abs
    rel_err = 100.to_f*abs_err/i
    puts "#{i} vs #{approx}: #{rel_err}%"
end
