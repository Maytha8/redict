# Redict

Redict is a distributed key/value store (a **re**mote **dict**ionary, if you
will).

Redict is an independent fork of Redis®\* OSS 7.2.4 licensed under the Lesser
GNU General Public license (LGPL-3.0-only). Redict is not affiliated with
Redis®.

## Installation and usage

For documentation on Redict's installation and usage, consult [the
website](https://redict.io). The intended audience of this README file are those
who wish to build Redict from source for development purposes.

## Building Redict from source

Redict only requires a compatible operating system (Unix-like), a standard C11
toolchain, and GNU make. To build Redict, simply run "make".

### Customizing the build

You can pass the following variables to Redict to customize the build:

* `USE_JEMALLOC=no MALLOC=libc`: use the libc allocator rather than jemalloc
* `BUILD_TLS=yes`: build with TLS support. Requires OpenSSL.
* `USE_SYSTEMD=yes`: build with systemd support. Requires libsystemd.
* `PROG_SUFFIX="-suffix"`: Append "-suffix" to executable names

### Running tests

To run the test suite, run `./runtest` after the build. The following
additional test suites are provided:

* `./runtest-cluster`: run cluster configuration tests
* `./runtest-moduleapi`: run Redict Module API tests
* `./runtest-sentinel`: run sentinel configuration tests

To run tests with TLS support enabled, build Redict with `BUILD_TLS=yes` and
generate test certificates with `./utils/gen-test-certs.sh`. You may then run
`./runtests --tls` to run the test suite with TLS testing enabled.

## Running Redict

After building Redict, simply run `./src/redict-server`. You may connect to it
with `./src/redict-cli`.

For details on TLS usage, consult TLS.md.

---

<small>* Redis is a registered trademark of Redis Ltd. Any rights therein are
reserved to Redis Ltd. Any use by the Redict project is for referential purposes
only and does not indicate any sponsorship, endorsement or affiliation between
Redis and the Redict project.</small>
