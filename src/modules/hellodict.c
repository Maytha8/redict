// Copyright (c) 2018, Salvatore Sanfilippo <antirez at gmail dot com>
// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause

#include "../redictmodule.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

static RedictModuleDict *Keyspace;

/* HELLODICT.SET <key> <value>
 *
 * Set the specified key to the specified value. */
int cmd_SET(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc != 3) return RedictModule_WrongArity(ctx);
    RedictModule_DictSet(Keyspace,argv[1],argv[2]);
    /* We need to keep a reference to the value stored at the key, otherwise
     * it would be freed when this callback returns. */
    RedictModule_RetainString(NULL,argv[2]);
    return RedictModule_ReplyWithSimpleString(ctx, "OK");
}

/* HELLODICT.GET <key>
 *
 * Return the value of the specified key, or a null reply if the key
 * is not defined. */
int cmd_GET(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc != 2) return RedictModule_WrongArity(ctx);
    RedictModuleString *val = RedictModule_DictGet(Keyspace,argv[1],NULL);
    if (val == NULL) {
        return RedictModule_ReplyWithNull(ctx);
    } else {
        return RedictModule_ReplyWithString(ctx, val);
    }
}

/* HELLODICT.KEYRANGE <startkey> <endkey> <count>
 *
 * Return a list of matching keys, lexicographically between startkey
 * and endkey. No more than 'count' items are emitted. */
int cmd_KEYRANGE(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc != 4) return RedictModule_WrongArity(ctx);

    /* Parse the count argument. */
    long long count;
    if (RedictModule_StringToLongLong(argv[3],&count) != REDICTMODULE_OK) {
        return RedictModule_ReplyWithError(ctx,"ERR invalid count");
    }

    /* Seek the iterator. */
    RedictModuleDictIter *iter = RedictModule_DictIteratorStart(
        Keyspace, ">=", argv[1]);

    /* Reply with the matching items. */
    char *key;
    size_t keylen;
    long long replylen = 0; /* Keep track of the emitted array len. */
    RedictModule_ReplyWithArray(ctx,REDICTMODULE_POSTPONED_LEN);
    while((key = RedictModule_DictNextC(iter,&keylen,NULL)) != NULL) {
        if (replylen >= count) break;
        if (RedictModule_DictCompare(iter,"<=",argv[2]) == REDICTMODULE_ERR)
            break;
        RedictModule_ReplyWithStringBuffer(ctx,key,keylen);
        replylen++;
    }
    RedictModule_ReplySetArrayLength(ctx,replylen);

    /* Cleanup. */
    RedictModule_DictIteratorStop(iter);
    return REDICTMODULE_OK;
}

/* This function must be present on each Redict module. It is used in order to
 * register the commands into the Redict server. */
int RedictModule_OnLoad(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    if (RedictModule_Init(ctx,"hellodict",1,REDICTMODULE_APIVER_1)
        == REDICTMODULE_ERR) return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"hellodict.set",
        cmd_SET,"write deny-oom",1,1,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"hellodict.get",
        cmd_GET,"readonly",1,1,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"hellodict.keyrange",
        cmd_KEYRANGE,"readonly",1,1,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    /* Create our global dictionary. Here we'll set our keys and values. */
    Keyspace = RedictModule_CreateDict(NULL);

    return REDICTMODULE_OK;
}
