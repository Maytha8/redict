// Copyright (c) 2018, Salvatore Sanfilippo <antirez at gmail dot com>
// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause

#include "../redictmodule.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define MSGTYPE_PING 1
#define MSGTYPE_PONG 2

/* HELLOCLUSTER.PINGALL */
int PingallCommand_RedictCommand(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    RedictModule_SendClusterMessage(ctx,NULL,MSGTYPE_PING,"Hey",3);
    return RedictModule_ReplyWithSimpleString(ctx, "OK");
}

/* HELLOCLUSTER.LIST */
int ListCommand_RedictCommand(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    size_t numnodes;
    char **ids = RedictModule_GetClusterNodesList(ctx,&numnodes);
    if (ids == NULL) {
        return RedictModule_ReplyWithError(ctx,"Cluster not enabled");
    }

    RedictModule_ReplyWithArray(ctx,numnodes);
    for (size_t j = 0; j < numnodes; j++) {
        int port;
        RedictModule_GetClusterNodeInfo(ctx,ids[j],NULL,NULL,&port,NULL);
        RedictModule_ReplyWithArray(ctx,2);
        RedictModule_ReplyWithStringBuffer(ctx,ids[j],REDICTMODULE_NODE_ID_LEN);
        RedictModule_ReplyWithLongLong(ctx,port);
    }
    RedictModule_FreeClusterNodesList(ids);
    return REDICTMODULE_OK;
}

/* Callback for message MSGTYPE_PING */
void PingReceiver(RedictModuleCtx *ctx, const char *sender_id, uint8_t type, const unsigned char *payload, uint32_t len) {
    RedictModule_Log(ctx,"notice","PING (type %d) RECEIVED from %.*s: '%.*s'",
        type,REDICTMODULE_NODE_ID_LEN,sender_id,(int)len, payload);
    RedictModule_SendClusterMessage(ctx,NULL,MSGTYPE_PONG,"Ohi!",4);
    RedictModuleCallReply *reply = RedictModule_Call(ctx, "INCR", "c", "pings_received");
    RedictModule_FreeCallReply(reply);
}

/* Callback for message MSGTYPE_PONG. */
void PongReceiver(RedictModuleCtx *ctx, const char *sender_id, uint8_t type, const unsigned char *payload, uint32_t len) {
    RedictModule_Log(ctx,"notice","PONG (type %d) RECEIVED from %.*s: '%.*s'",
        type,REDICTMODULE_NODE_ID_LEN,sender_id,(int)len, payload);
}

/* This function must be present on each Redict module. It is used in order to
 * register the commands into the Redict server. */
int RedictModule_OnLoad(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    if (RedictModule_Init(ctx,"hellocluster",1,REDICTMODULE_APIVER_1)
        == REDICTMODULE_ERR) return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"hellocluster.pingall",
        PingallCommand_RedictCommand,"readonly",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"hellocluster.list",
        ListCommand_RedictCommand,"readonly",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    /* Disable Redict Cluster sharding and redirections. This way every node
     * will be able to access every possible key, regardless of the hash slot.
     * This way the PING message handler will be able to increment a specific
     * variable. Normally you do that in order for the distributed system
     * you create as a module to have total freedom in the keyspace
     * manipulation. */
    RedictModule_SetClusterFlags(ctx,REDICTMODULE_CLUSTER_FLAG_NO_REDIRECTION);

    /* Register our handlers for different message types. */
    RedictModule_RegisterClusterMessageReceiver(ctx,MSGTYPE_PING,PingReceiver);
    RedictModule_RegisterClusterMessageReceiver(ctx,MSGTYPE_PONG,PongReceiver);
    return REDICTMODULE_OK;
}
