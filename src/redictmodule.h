// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef REDICTMODULE_H
#define REDICTMODULE_H

#include <sys/types.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


typedef struct RedictModuleString RedictModuleString;
typedef struct RedictModuleKey RedictModuleKey;

/* -------------- Defines NOT common between core and modules ------------- */

#if defined REDICTMODULE_CORE
/* Things only defined for the modules core (server), not exported to modules
 * that include this file. */

#define RedictModuleString robj

#endif /* defined REDICTMODULE_CORE */

#if !defined REDICTMODULE_CORE && !defined REDICTMODULE_CORE_MODULE
/* Things defined for modules, but not for core-modules. */

typedef long long mstime_t;
typedef long long ustime_t;

#endif /* !defined REDICTMODULE_CORE && !defined REDICTMODULE_CORE_MODULE */

/* ---------------- Defines common between core and modules --------------- */

/* Error status return values. */
#define REDICTMODULE_OK 0
#define REDICTMODULE_ERR 1

/* Module Based Authentication status return values. */
#define REDICTMODULE_AUTH_HANDLED 0
#define REDICTMODULE_AUTH_NOT_HANDLED 1

/* API versions. */
#define REDICTMODULE_APIVER_1 1

/* Version of the RedictModuleTypeMethods structure. Once the RedictModuleTypeMethods
 * structure is changed, this version number needs to be changed synchronistically. */
#define REDICTMODULE_TYPE_METHOD_VERSION 5

/* API flags and constants */
#define REDICTMODULE_READ (1<<0)
#define REDICTMODULE_WRITE (1<<1)

/* RedictModule_OpenKey extra flags for the 'mode' argument.
 * Avoid touching the LRU/LFU of the key when opened. */
#define REDICTMODULE_OPEN_KEY_NOTOUCH (1<<16)
/* Don't trigger keyspace event on key misses. */
#define REDICTMODULE_OPEN_KEY_NONOTIFY (1<<17)
/* Don't update keyspace hits/misses counters. */
#define REDICTMODULE_OPEN_KEY_NOSTATS (1<<18)
/* Avoid deleting lazy expired keys. */
#define REDICTMODULE_OPEN_KEY_NOEXPIRE (1<<19)
/* Avoid any effects from fetching the key */
#define REDICTMODULE_OPEN_KEY_NOEFFECTS (1<<20)
/* Mask of all REDICTMODULE_OPEN_KEY_* values. Any new mode should be added to this list.
 * Should not be used directly by the module, use RM_GetOpenKeyModesAll instead.
 * Located here so when we will add new modes we will not forget to update it. */
#define _REDICTMODULE_OPEN_KEY_ALL REDICTMODULE_READ | REDICTMODULE_WRITE | REDICTMODULE_OPEN_KEY_NOTOUCH | REDICTMODULE_OPEN_KEY_NONOTIFY | REDICTMODULE_OPEN_KEY_NOSTATS | REDICTMODULE_OPEN_KEY_NOEXPIRE | REDICTMODULE_OPEN_KEY_NOEFFECTS

/* List push and pop */
#define REDICTMODULE_LIST_HEAD 0
#define REDICTMODULE_LIST_TAIL 1

/* Key types. */
#define REDICTMODULE_KEYTYPE_EMPTY 0
#define REDICTMODULE_KEYTYPE_STRING 1
#define REDICTMODULE_KEYTYPE_LIST 2
#define REDICTMODULE_KEYTYPE_HASH 3
#define REDICTMODULE_KEYTYPE_SET 4
#define REDICTMODULE_KEYTYPE_ZSET 5
#define REDICTMODULE_KEYTYPE_MODULE 6
#define REDICTMODULE_KEYTYPE_STREAM 7

/* Reply types. */
#define REDICTMODULE_REPLY_UNKNOWN -1
#define REDICTMODULE_REPLY_STRING 0
#define REDICTMODULE_REPLY_ERROR 1
#define REDICTMODULE_REPLY_INTEGER 2
#define REDICTMODULE_REPLY_ARRAY 3
#define REDICTMODULE_REPLY_NULL 4
#define REDICTMODULE_REPLY_MAP 5
#define REDICTMODULE_REPLY_SET 6
#define REDICTMODULE_REPLY_BOOL 7
#define REDICTMODULE_REPLY_DOUBLE 8
#define REDICTMODULE_REPLY_BIG_NUMBER 9
#define REDICTMODULE_REPLY_VERBATIM_STRING 10
#define REDICTMODULE_REPLY_ATTRIBUTE 11
#define REDICTMODULE_REPLY_PROMISE 12

/* Postponed array length. */
#define REDICTMODULE_POSTPONED_ARRAY_LEN -1  /* Deprecated, please use REDICTMODULE_POSTPONED_LEN */
#define REDICTMODULE_POSTPONED_LEN -1

/* Expire */
#define REDICTMODULE_NO_EXPIRE -1

/* Sorted set API flags. */
#define REDICTMODULE_ZADD_XX      (1<<0)
#define REDICTMODULE_ZADD_NX      (1<<1)
#define REDICTMODULE_ZADD_ADDED   (1<<2)
#define REDICTMODULE_ZADD_UPDATED (1<<3)
#define REDICTMODULE_ZADD_NOP     (1<<4)
#define REDICTMODULE_ZADD_GT      (1<<5)
#define REDICTMODULE_ZADD_LT      (1<<6)

/* Hash API flags. */
#define REDICTMODULE_HASH_NONE       0
#define REDICTMODULE_HASH_NX         (1<<0)
#define REDICTMODULE_HASH_XX         (1<<1)
#define REDICTMODULE_HASH_CFIELDS    (1<<2)
#define REDICTMODULE_HASH_EXISTS     (1<<3)
#define REDICTMODULE_HASH_COUNT_ALL  (1<<4)

#define REDICTMODULE_CONFIG_DEFAULT 0 /* This is the default for a module config. */
#define REDICTMODULE_CONFIG_IMMUTABLE (1ULL<<0) /* Can this value only be set at startup? */
#define REDICTMODULE_CONFIG_SENSITIVE (1ULL<<1) /* Does this value contain sensitive information */
#define REDICTMODULE_CONFIG_HIDDEN (1ULL<<4) /* This config is hidden in `config get <pattern>` (used for tests/debugging) */
#define REDICTMODULE_CONFIG_PROTECTED (1ULL<<5) /* Becomes immutable if enable-protected-configs is enabled. */
#define REDICTMODULE_CONFIG_DENY_LOADING (1ULL<<6) /* This config is forbidden during loading. */

#define REDICTMODULE_CONFIG_MEMORY (1ULL<<7) /* Indicates if this value can be set as a memory value */
#define REDICTMODULE_CONFIG_BITFLAGS (1ULL<<8) /* Indicates if this value can be set as a multiple enum values */

/* StreamID type. */
typedef struct RedictModuleStreamID {
    uint64_t ms;
    uint64_t seq;
} RedictModuleStreamID;

/* StreamAdd() flags. */
#define REDICTMODULE_STREAM_ADD_AUTOID (1<<0)
/* StreamIteratorStart() flags. */
#define REDICTMODULE_STREAM_ITERATOR_EXCLUSIVE (1<<0)
#define REDICTMODULE_STREAM_ITERATOR_REVERSE (1<<1)
/* StreamIteratorTrim*() flags. */
#define REDICTMODULE_STREAM_TRIM_APPROX (1<<0)

/* Context Flags: Info about the current context returned by
 * RM_GetContextFlags(). */

/* The command is running in the context of a Lua script */
#define REDICTMODULE_CTX_FLAGS_LUA (1<<0)
/* The command is running inside a Redict transaction */
#define REDICTMODULE_CTX_FLAGS_MULTI (1<<1)
/* The instance is a master */
#define REDICTMODULE_CTX_FLAGS_MASTER (1<<2)
/* The instance is a slave */
#define REDICTMODULE_CTX_FLAGS_SLAVE (1<<3)
/* The instance is read-only (usually meaning it's a slave as well) */
#define REDICTMODULE_CTX_FLAGS_READONLY (1<<4)
/* The instance is running in cluster mode */
#define REDICTMODULE_CTX_FLAGS_CLUSTER (1<<5)
/* The instance has AOF enabled */
#define REDICTMODULE_CTX_FLAGS_AOF (1<<6)
/* The instance has RDB enabled */
#define REDICTMODULE_CTX_FLAGS_RDB (1<<7)
/* The instance has Maxmemory set */
#define REDICTMODULE_CTX_FLAGS_MAXMEMORY (1<<8)
/* Maxmemory is set and has an eviction policy that may delete keys */
#define REDICTMODULE_CTX_FLAGS_EVICT (1<<9)
/* Redict is out of memory according to the maxmemory flag. */
#define REDICTMODULE_CTX_FLAGS_OOM (1<<10)
/* Less than 25% of memory available according to maxmemory. */
#define REDICTMODULE_CTX_FLAGS_OOM_WARNING (1<<11)
/* The command was sent over the replication link. */
#define REDICTMODULE_CTX_FLAGS_REPLICATED (1<<12)
/* Redict is currently loading either from AOF or RDB. */
#define REDICTMODULE_CTX_FLAGS_LOADING (1<<13)
/* The replica has no link with its master, note that
 * there is the inverse flag as well:
 *
 *  REDICTMODULE_CTX_FLAGS_REPLICA_IS_ONLINE
 *
 * The two flags are exclusive, one or the other can be set. */
#define REDICTMODULE_CTX_FLAGS_REPLICA_IS_STALE (1<<14)
/* The replica is trying to connect with the master.
 * (REPL_STATE_CONNECT and REPL_STATE_CONNECTING states) */
#define REDICTMODULE_CTX_FLAGS_REPLICA_IS_CONNECTING (1<<15)
/* THe replica is receiving an RDB file from its master. */
#define REDICTMODULE_CTX_FLAGS_REPLICA_IS_TRANSFERRING (1<<16)
/* The replica is online, receiving updates from its master. */
#define REDICTMODULE_CTX_FLAGS_REPLICA_IS_ONLINE (1<<17)
/* There is currently some background process active. */
#define REDICTMODULE_CTX_FLAGS_ACTIVE_CHILD (1<<18)
/* The next EXEC will fail due to dirty CAS (touched keys). */
#define REDICTMODULE_CTX_FLAGS_MULTI_DIRTY (1<<19)
/* Redict is currently running inside background child process. */
#define REDICTMODULE_CTX_FLAGS_IS_CHILD (1<<20)
/* The current client does not allow blocking, either called from
 * within multi, lua, or from another module using RM_Call */
#define REDICTMODULE_CTX_FLAGS_DENY_BLOCKING (1<<21)
/* The current client uses RESP3 protocol */
#define REDICTMODULE_CTX_FLAGS_RESP3 (1<<22)
/* Redict is currently async loading database for diskless replication. */
#define REDICTMODULE_CTX_FLAGS_ASYNC_LOADING (1<<23)
/* Redict is starting. */
#define REDICTMODULE_CTX_FLAGS_SERVER_STARTUP (1<<24)

/* Next context flag, must be updated when adding new flags above!
This flag should not be used directly by the module.
 * Use RedictModule_GetContextFlagsAll instead. */
#define _REDICTMODULE_CTX_FLAGS_NEXT (1<<25)

/* Keyspace changes notification classes. Every class is associated with a
 * character for configuration purposes.
 * NOTE: These have to be in sync with NOTIFY_* in server.h */
#define REDICTMODULE_NOTIFY_KEYSPACE (1<<0)    /* K */
#define REDICTMODULE_NOTIFY_KEYEVENT (1<<1)    /* E */
#define REDICTMODULE_NOTIFY_GENERIC (1<<2)     /* g */
#define REDICTMODULE_NOTIFY_STRING (1<<3)      /* $ */
#define REDICTMODULE_NOTIFY_LIST (1<<4)        /* l */
#define REDICTMODULE_NOTIFY_SET (1<<5)         /* s */
#define REDICTMODULE_NOTIFY_HASH (1<<6)        /* h */
#define REDICTMODULE_NOTIFY_ZSET (1<<7)        /* z */
#define REDICTMODULE_NOTIFY_EXPIRED (1<<8)     /* x */
#define REDICTMODULE_NOTIFY_EVICTED (1<<9)     /* e */
#define REDICTMODULE_NOTIFY_STREAM (1<<10)     /* t */
#define REDICTMODULE_NOTIFY_KEY_MISS (1<<11)   /* m (Note: This one is excluded from REDICTMODULE_NOTIFY_ALL on purpose) */
#define REDICTMODULE_NOTIFY_LOADED (1<<12)     /* module only key space notification, indicate a key loaded from rdb */
#define REDICTMODULE_NOTIFY_MODULE (1<<13)     /* d, module key space notification */
#define REDICTMODULE_NOTIFY_NEW (1<<14)        /* n, new key notification */

/* Next notification flag, must be updated when adding new flags above!
This flag should not be used directly by the module.
 * Use RedictModule_GetKeyspaceNotificationFlagsAll instead. */
#define _REDICTMODULE_NOTIFY_NEXT (1<<15)

#define REDICTMODULE_NOTIFY_ALL (REDICTMODULE_NOTIFY_GENERIC | REDICTMODULE_NOTIFY_STRING | REDICTMODULE_NOTIFY_LIST | REDICTMODULE_NOTIFY_SET | REDICTMODULE_NOTIFY_HASH | REDICTMODULE_NOTIFY_ZSET | REDICTMODULE_NOTIFY_EXPIRED | REDICTMODULE_NOTIFY_EVICTED | REDICTMODULE_NOTIFY_STREAM | REDICTMODULE_NOTIFY_MODULE)      /* A */

/* A special pointer that we can use between the core and the module to signal
 * field deletion, and that is impossible to be a valid pointer. */
#define REDICTMODULE_HASH_DELETE ((RedictModuleString*)(long)1)

/* Error messages. */
#define REDICTMODULE_ERRORMSG_WRONGTYPE "WRONGTYPE Operation against a key holding the wrong kind of value"

#define REDICTMODULE_POSITIVE_INFINITE (1.0/0.0)
#define REDICTMODULE_NEGATIVE_INFINITE (-1.0/0.0)

/* Cluster API defines. */
#define REDICTMODULE_NODE_ID_LEN 40
#define REDICTMODULE_NODE_MYSELF     (1<<0)
#define REDICTMODULE_NODE_MASTER     (1<<1)
#define REDICTMODULE_NODE_SLAVE      (1<<2)
#define REDICTMODULE_NODE_PFAIL      (1<<3)
#define REDICTMODULE_NODE_FAIL       (1<<4)
#define REDICTMODULE_NODE_NOFAILOVER (1<<5)

#define REDICTMODULE_CLUSTER_FLAG_NONE 0
#define REDICTMODULE_CLUSTER_FLAG_NO_FAILOVER (1<<1)
#define REDICTMODULE_CLUSTER_FLAG_NO_REDIRECTION (1<<2)

#define REDICTMODULE_NOT_USED(V) ((void) V)

/* Logging level strings */
#define REDICTMODULE_LOGLEVEL_DEBUG "debug"
#define REDICTMODULE_LOGLEVEL_VERBOSE "verbose"
#define REDICTMODULE_LOGLEVEL_NOTICE "notice"
#define REDICTMODULE_LOGLEVEL_WARNING "warning"

/* Bit flags for aux_save_triggers and the aux_load and aux_save callbacks */
#define REDICTMODULE_AUX_BEFORE_RDB (1<<0)
#define REDICTMODULE_AUX_AFTER_RDB (1<<1)

/* RM_Yield flags */
#define REDICTMODULE_YIELD_FLAG_NONE (1<<0)
#define REDICTMODULE_YIELD_FLAG_CLIENTS (1<<1)

/* RM_BlockClientOnKeysWithFlags flags */
#define REDICTMODULE_BLOCK_UNBLOCK_DEFAULT (0)
#define REDICTMODULE_BLOCK_UNBLOCK_DELETED (1<<0)

/* This type represents a timer handle, and is returned when a timer is
 * registered and used in order to invalidate a timer. It's just a 64 bit
 * number, because this is how each timer is represented inside the radix tree
 * of timers that are going to expire, sorted by expire time. */
typedef uint64_t RedictModuleTimerID;

/* CommandFilter Flags */

/* Do filter RedictModule_Call() commands initiated by module itself. */
#define REDICTMODULE_CMDFILTER_NOSELF    (1<<0)

/* Declare that the module can handle errors with RedictModule_SetModuleOptions. */
#define REDICTMODULE_OPTIONS_HANDLE_IO_ERRORS    (1<<0)

/* When set, Redict will not call RedictModule_SignalModifiedKey(), implicitly in
 * RedictModule_CloseKey, and the module needs to do that when manually when keys
 * are modified from the user's perspective, to invalidate WATCH. */
#define REDICTMODULE_OPTION_NO_IMPLICIT_SIGNAL_MODIFIED (1<<1)

/* Declare that the module can handle diskless async replication with RedictModule_SetModuleOptions. */
#define REDICTMODULE_OPTIONS_HANDLE_REPL_ASYNC_LOAD    (1<<2)

/* Declare that the module want to get nested key space notifications.
 * If enabled, the module is responsible to break endless loop. */
#define REDICTMODULE_OPTIONS_ALLOW_NESTED_KEYSPACE_NOTIFICATIONS    (1<<3)

/* Next option flag, must be updated when adding new module flags above!
 * This flag should not be used directly by the module.
 * Use RedictModule_GetModuleOptionsAll instead. */
#define _REDICTMODULE_OPTIONS_FLAGS_NEXT (1<<4)

/* Definitions for RedictModule_SetCommandInfo. */

typedef enum {
    REDICTMODULE_ARG_TYPE_STRING,
    REDICTMODULE_ARG_TYPE_INTEGER,
    REDICTMODULE_ARG_TYPE_DOUBLE,
    REDICTMODULE_ARG_TYPE_KEY, /* A string, but represents a keyname */
    REDICTMODULE_ARG_TYPE_PATTERN,
    REDICTMODULE_ARG_TYPE_UNIX_TIME,
    REDICTMODULE_ARG_TYPE_PURE_TOKEN,
    REDICTMODULE_ARG_TYPE_ONEOF, /* Must have sub-arguments */
    REDICTMODULE_ARG_TYPE_BLOCK /* Must have sub-arguments */
} RedictModuleCommandArgType;

#define REDICTMODULE_CMD_ARG_NONE            (0)
#define REDICTMODULE_CMD_ARG_OPTIONAL        (1<<0) /* The argument is optional (like GET in SET command) */
#define REDICTMODULE_CMD_ARG_MULTIPLE        (1<<1) /* The argument may repeat itself (like key in DEL) */
#define REDICTMODULE_CMD_ARG_MULTIPLE_TOKEN  (1<<2) /* The argument may repeat itself, and so does its token (like `GET pattern` in SORT) */
#define _REDICTMODULE_CMD_ARG_NEXT           (1<<3)

typedef enum {
    REDICTMODULE_KSPEC_BS_INVALID = 0, /* Must be zero. An implicitly value of
                                       * zero is provided when the field is
                                       * absent in a struct literal. */
    REDICTMODULE_KSPEC_BS_UNKNOWN,
    REDICTMODULE_KSPEC_BS_INDEX,
    REDICTMODULE_KSPEC_BS_KEYWORD
} RedictModuleKeySpecBeginSearchType;

typedef enum {
    REDICTMODULE_KSPEC_FK_OMITTED = 0, /* Used when the field is absent in a
                                       * struct literal. Don't use this value
                                       * explicitly. */
    REDICTMODULE_KSPEC_FK_UNKNOWN,
    REDICTMODULE_KSPEC_FK_RANGE,
    REDICTMODULE_KSPEC_FK_KEYNUM
} RedictModuleKeySpecFindKeysType;

/* Key-spec flags. For details, see the documentation of
 * RedictModule_SetCommandInfo and the key-spec flags in server.h. */
#define REDICTMODULE_CMD_KEY_RO (1ULL<<0)
#define REDICTMODULE_CMD_KEY_RW (1ULL<<1)
#define REDICTMODULE_CMD_KEY_OW (1ULL<<2)
#define REDICTMODULE_CMD_KEY_RM (1ULL<<3)
#define REDICTMODULE_CMD_KEY_ACCESS (1ULL<<4)
#define REDICTMODULE_CMD_KEY_UPDATE (1ULL<<5)
#define REDICTMODULE_CMD_KEY_INSERT (1ULL<<6)
#define REDICTMODULE_CMD_KEY_DELETE (1ULL<<7)
#define REDICTMODULE_CMD_KEY_NOT_KEY (1ULL<<8)
#define REDICTMODULE_CMD_KEY_INCOMPLETE (1ULL<<9)
#define REDICTMODULE_CMD_KEY_VARIABLE_FLAGS (1ULL<<10)

/* Channel flags, for details see the documentation of
 * RedictModule_ChannelAtPosWithFlags. */
#define REDICTMODULE_CMD_CHANNEL_PATTERN (1ULL<<0)
#define REDICTMODULE_CMD_CHANNEL_PUBLISH (1ULL<<1)
#define REDICTMODULE_CMD_CHANNEL_SUBSCRIBE (1ULL<<2)
#define REDICTMODULE_CMD_CHANNEL_UNSUBSCRIBE (1ULL<<3)

typedef struct RedictModuleCommandArg {
    const char *name;
    RedictModuleCommandArgType type;
    int key_spec_index;       /* If type is KEY, this is a zero-based index of
                               * the key_spec in the command. For other types,
                               * you may specify -1. */
    const char *token;        /* If type is PURE_TOKEN, this is the token. */
    const char *summary;
    const char *since;
    int flags;                /* The REDICTMODULE_CMD_ARG_* macros. */
    const char *deprecated_since;
    struct RedictModuleCommandArg *subargs;
    const char *display_text;
} RedictModuleCommandArg;

typedef struct {
    const char *since;
    const char *changes;
} RedictModuleCommandHistoryEntry;

typedef struct {
    const char *notes;
    uint64_t flags; /* REDICTMODULE_CMD_KEY_* macros. */
    RedictModuleKeySpecBeginSearchType begin_search_type;
    union {
        struct {
            /* The index from which we start the search for keys */
            int pos;
        } index;
        struct {
            /* The keyword that indicates the beginning of key args */
            const char *keyword;
            /* An index in argv from which to start searching.
             * Can be negative, which means start search from the end, in reverse
             * (Example: -2 means to start in reverse from the penultimate arg) */
            int startfrom;
        } keyword;
    } bs;
    RedictModuleKeySpecFindKeysType find_keys_type;
    union {
        struct {
            /* Index of the last key relative to the result of the begin search
             * step. Can be negative, in which case it's not relative. -1
             * indicating till the last argument, -2 one before the last and so
             * on. */
            int lastkey;
            /* How many args should we skip after finding a key, in order to
             * find the next one. */
            int keystep;
            /* If lastkey is -1, we use limit to stop the search by a factor. 0
             * and 1 mean no limit. 2 means 1/2 of the remaining args, 3 means
             * 1/3, and so on. */
            int limit;
        } range;
        struct {
            /* Index of the argument containing the number of keys to come
             * relative to the result of the begin search step */
            int keynumidx;
            /* Index of the fist key. (Usually it's just after keynumidx, in
             * which case it should be set to keynumidx + 1.) */
            int firstkey;
            /* How many args should we skip after finding a key, in order to
             * find the next one, relative to the result of the begin search
             * step. */
            int keystep;
        } keynum;
    } fk;
} RedictModuleCommandKeySpec;

typedef struct {
    int version;
    size_t sizeof_historyentry;
    size_t sizeof_keyspec;
    size_t sizeof_arg;
} RedictModuleCommandInfoVersion;

static const RedictModuleCommandInfoVersion RedictModule_CurrentCommandInfoVersion = {
    .version = 1,
    .sizeof_historyentry = sizeof(RedictModuleCommandHistoryEntry),
    .sizeof_keyspec = sizeof(RedictModuleCommandKeySpec),
    .sizeof_arg = sizeof(RedictModuleCommandArg)
};

#define REDICTMODULE_COMMAND_INFO_VERSION (&RedictModule_CurrentCommandInfoVersion)

typedef struct {
    /* Always set version to REDICTMODULE_COMMAND_INFO_VERSION */
    const RedictModuleCommandInfoVersion *version;
    /* Version 1 fields (added in Redis 7.0.0) */
    const char *summary;          /* Summary of the command */
    const char *complexity;       /* Complexity description */
    const char *since;            /* Debut module version of the command */
    RedictModuleCommandHistoryEntry *history; /* History */
    /* A string of space-separated tips meant for clients/proxies regarding this
     * command */
    const char *tips;
    /* Number of arguments, it is possible to use -N to say >= N */
    int arity;
    RedictModuleCommandKeySpec *key_specs;
    RedictModuleCommandArg *args;
} RedictModuleCommandInfo;

/* Eventloop definitions. */
#define REDICTMODULE_EVENTLOOP_READABLE 1
#define REDICTMODULE_EVENTLOOP_WRITABLE 2
typedef void (*RedictModuleEventLoopFunc)(int fd, void *user_data, int mask);
typedef void (*RedictModuleEventLoopOneShotFunc)(void *user_data);

/* Server events definitions.
 * Those flags should not be used directly by the module, instead
 * the module should use RedictModuleEvent_* variables.
 * Note: This must be synced with moduleEventVersions */
#define REDICTMODULE_EVENT_REPLICATION_ROLE_CHANGED 0
#define REDICTMODULE_EVENT_PERSISTENCE 1
#define REDICTMODULE_EVENT_FLUSHDB 2
#define REDICTMODULE_EVENT_LOADING 3
#define REDICTMODULE_EVENT_CLIENT_CHANGE 4
#define REDICTMODULE_EVENT_SHUTDOWN 5
#define REDICTMODULE_EVENT_REPLICA_CHANGE 6
#define REDICTMODULE_EVENT_MASTER_LINK_CHANGE 7
#define REDICTMODULE_EVENT_CRON_LOOP 8
#define REDICTMODULE_EVENT_MODULE_CHANGE 9
#define REDICTMODULE_EVENT_LOADING_PROGRESS 10
#define REDICTMODULE_EVENT_SWAPDB 11
#define REDICTMODULE_EVENT_REPL_BACKUP 12 /* Deprecated since Redis 7.0, not used anymore. */
#define REDICTMODULE_EVENT_FORK_CHILD 13
#define REDICTMODULE_EVENT_REPL_ASYNC_LOAD 14
#define REDICTMODULE_EVENT_EVENTLOOP 15
#define REDICTMODULE_EVENT_CONFIG 16
#define REDICTMODULE_EVENT_KEY 17
#define _REDICTMODULE_EVENT_NEXT 18 /* Next event flag, should be updated if a new event added. */

typedef struct RedictModuleEvent {
    uint64_t id;        /* REDICTMODULE_EVENT_... defines. */
    uint64_t dataver;   /* Version of the structure we pass as 'data'. */
} RedictModuleEvent;

struct RedictModuleCtx;
struct RedictModuleDefragCtx;
typedef void (*RedictModuleEventCallback)(struct RedictModuleCtx *ctx, RedictModuleEvent eid, uint64_t subevent, void *data);

/* IMPORTANT: When adding a new version of one of below structures that contain
 * event data (RedictModuleFlushInfoV1 for example) we have to avoid renaming the
 * old RedictModuleEvent structure.
 * For example, if we want to add RedictModuleFlushInfoV2, the RedictModuleEvent
 * structures should be:
 *      RedictModuleEvent_FlushDB = {
 *          REDICTMODULE_EVENT_FLUSHDB,
 *          1
 *      },
 *      RedictModuleEvent_FlushDBV2 = {
 *          REDICTMODULE_EVENT_FLUSHDB,
 *          2
 *      }
 * and NOT:
 *      RedictModuleEvent_FlushDBV1 = {
 *          REDICTMODULE_EVENT_FLUSHDB,
 *          1
 *      },
 *      RedictModuleEvent_FlushDB = {
 *          REDICTMODULE_EVENT_FLUSHDB,
 *          2
 *      }
 * The reason for that is forward-compatibility: We want that module that
 * compiled with a new redictmodule.h to be able to work with a old server,
 * unless the author explicitly decided to use the newer event type.
 */
static const RedictModuleEvent
    RedictModuleEvent_ReplicationRoleChanged = {
        REDICTMODULE_EVENT_REPLICATION_ROLE_CHANGED,
        1
    },
    RedictModuleEvent_Persistence = {
        REDICTMODULE_EVENT_PERSISTENCE,
        1
    },
    RedictModuleEvent_FlushDB = {
        REDICTMODULE_EVENT_FLUSHDB,
        1
    },
    RedictModuleEvent_Loading = {
        REDICTMODULE_EVENT_LOADING,
        1
    },
    RedictModuleEvent_ClientChange = {
        REDICTMODULE_EVENT_CLIENT_CHANGE,
        1
    },
    RedictModuleEvent_Shutdown = {
        REDICTMODULE_EVENT_SHUTDOWN,
        1
    },
    RedictModuleEvent_ReplicaChange = {
        REDICTMODULE_EVENT_REPLICA_CHANGE,
        1
    },
    RedictModuleEvent_CronLoop = {
        REDICTMODULE_EVENT_CRON_LOOP,
        1
    },
    RedictModuleEvent_MasterLinkChange = {
        REDICTMODULE_EVENT_MASTER_LINK_CHANGE,
        1
    },
    RedictModuleEvent_ModuleChange = {
        REDICTMODULE_EVENT_MODULE_CHANGE,
        1
    },
    RedictModuleEvent_LoadingProgress = {
        REDICTMODULE_EVENT_LOADING_PROGRESS,
        1
    },
    RedictModuleEvent_SwapDB = {
        REDICTMODULE_EVENT_SWAPDB,
        1
    },
    /* Deprecated since Redis 7.0, not used anymore. */
    __attribute__ ((deprecated))
    RedictModuleEvent_ReplBackup = {
        REDICTMODULE_EVENT_REPL_BACKUP,
        1
    },
    RedictModuleEvent_ReplAsyncLoad = {
        REDICTMODULE_EVENT_REPL_ASYNC_LOAD,
        1
    },
    RedictModuleEvent_ForkChild = {
        REDICTMODULE_EVENT_FORK_CHILD,
        1
    },
    RedictModuleEvent_EventLoop = {
        REDICTMODULE_EVENT_EVENTLOOP,
        1
    },
    RedictModuleEvent_Config = {
        REDICTMODULE_EVENT_CONFIG,
        1
    },
    RedictModuleEvent_Key = {
        REDICTMODULE_EVENT_KEY,
        1
    };

/* Those are values that are used for the 'subevent' callback argument. */
#define REDICTMODULE_SUBEVENT_PERSISTENCE_RDB_START 0
#define REDICTMODULE_SUBEVENT_PERSISTENCE_AOF_START 1
#define REDICTMODULE_SUBEVENT_PERSISTENCE_SYNC_RDB_START 2
#define REDICTMODULE_SUBEVENT_PERSISTENCE_ENDED 3
#define REDICTMODULE_SUBEVENT_PERSISTENCE_FAILED 4
#define REDICTMODULE_SUBEVENT_PERSISTENCE_SYNC_AOF_START 5
#define _REDICTMODULE_SUBEVENT_PERSISTENCE_NEXT 6

#define REDICTMODULE_SUBEVENT_LOADING_RDB_START 0
#define REDICTMODULE_SUBEVENT_LOADING_AOF_START 1
#define REDICTMODULE_SUBEVENT_LOADING_REPL_START 2
#define REDICTMODULE_SUBEVENT_LOADING_ENDED 3
#define REDICTMODULE_SUBEVENT_LOADING_FAILED 4
#define _REDICTMODULE_SUBEVENT_LOADING_NEXT 5

#define REDICTMODULE_SUBEVENT_CLIENT_CHANGE_CONNECTED 0
#define REDICTMODULE_SUBEVENT_CLIENT_CHANGE_DISCONNECTED 1
#define _REDICTMODULE_SUBEVENT_CLIENT_CHANGE_NEXT 2

#define REDICTMODULE_SUBEVENT_MASTER_LINK_UP 0
#define REDICTMODULE_SUBEVENT_MASTER_LINK_DOWN 1
#define _REDICTMODULE_SUBEVENT_MASTER_NEXT 2

#define REDICTMODULE_SUBEVENT_REPLICA_CHANGE_ONLINE 0
#define REDICTMODULE_SUBEVENT_REPLICA_CHANGE_OFFLINE 1
#define _REDICTMODULE_SUBEVENT_REPLICA_CHANGE_NEXT 2

#define REDICTMODULE_EVENT_REPLROLECHANGED_NOW_MASTER 0
#define REDICTMODULE_EVENT_REPLROLECHANGED_NOW_REPLICA 1
#define _REDICTMODULE_EVENT_REPLROLECHANGED_NEXT 2

#define REDICTMODULE_SUBEVENT_FLUSHDB_START 0
#define REDICTMODULE_SUBEVENT_FLUSHDB_END 1
#define _REDICTMODULE_SUBEVENT_FLUSHDB_NEXT 2

#define REDICTMODULE_SUBEVENT_MODULE_LOADED 0
#define REDICTMODULE_SUBEVENT_MODULE_UNLOADED 1
#define _REDICTMODULE_SUBEVENT_MODULE_NEXT 2

#define REDICTMODULE_SUBEVENT_CONFIG_CHANGE 0
#define _REDICTMODULE_SUBEVENT_CONFIG_NEXT 1

#define REDICTMODULE_SUBEVENT_LOADING_PROGRESS_RDB 0
#define REDICTMODULE_SUBEVENT_LOADING_PROGRESS_AOF 1
#define _REDICTMODULE_SUBEVENT_LOADING_PROGRESS_NEXT 2

/* Replication Backup events are deprecated since Redis 7.0 and are never fired. */
#define REDICTMODULE_SUBEVENT_REPL_BACKUP_CREATE 0
#define REDICTMODULE_SUBEVENT_REPL_BACKUP_RESTORE 1
#define REDICTMODULE_SUBEVENT_REPL_BACKUP_DISCARD 2
#define _REDICTMODULE_SUBEVENT_REPL_BACKUP_NEXT 3

#define REDICTMODULE_SUBEVENT_REPL_ASYNC_LOAD_STARTED 0
#define REDICTMODULE_SUBEVENT_REPL_ASYNC_LOAD_ABORTED 1
#define REDICTMODULE_SUBEVENT_REPL_ASYNC_LOAD_COMPLETED 2
#define _REDICTMODULE_SUBEVENT_REPL_ASYNC_LOAD_NEXT 3

#define REDICTMODULE_SUBEVENT_FORK_CHILD_BORN 0
#define REDICTMODULE_SUBEVENT_FORK_CHILD_DIED 1
#define _REDICTMODULE_SUBEVENT_FORK_CHILD_NEXT 2

#define REDICTMODULE_SUBEVENT_EVENTLOOP_BEFORE_SLEEP 0
#define REDICTMODULE_SUBEVENT_EVENTLOOP_AFTER_SLEEP 1
#define _REDICTMODULE_SUBEVENT_EVENTLOOP_NEXT 2

#define REDICTMODULE_SUBEVENT_KEY_DELETED 0
#define REDICTMODULE_SUBEVENT_KEY_EXPIRED 1
#define REDICTMODULE_SUBEVENT_KEY_EVICTED 2
#define REDICTMODULE_SUBEVENT_KEY_OVERWRITTEN 3
#define _REDICTMODULE_SUBEVENT_KEY_NEXT 4

#define _REDICTMODULE_SUBEVENT_SHUTDOWN_NEXT 0
#define _REDICTMODULE_SUBEVENT_CRON_LOOP_NEXT 0
#define _REDICTMODULE_SUBEVENT_SWAPDB_NEXT 0

/* RedictModuleClientInfo flags. */
#define REDICTMODULE_CLIENTINFO_FLAG_SSL (1<<0)
#define REDICTMODULE_CLIENTINFO_FLAG_PUBSUB (1<<1)
#define REDICTMODULE_CLIENTINFO_FLAG_BLOCKED (1<<2)
#define REDICTMODULE_CLIENTINFO_FLAG_TRACKING (1<<3)
#define REDICTMODULE_CLIENTINFO_FLAG_UNIXSOCKET (1<<4)
#define REDICTMODULE_CLIENTINFO_FLAG_MULTI (1<<5)

/* Here we take all the structures that the module pass to the core
 * and the other way around. Notably the list here contains the structures
 * used by the hooks API RedictModule_RegisterToServerEvent().
 *
 * The structures always start with a 'version' field. This is useful
 * when we want to pass a reference to the structure to the core APIs,
 * for the APIs to fill the structure. In that case, the structure 'version'
 * field is initialized before passing it to the core, so that the core is
 * able to cast the pointer to the appropriate structure version. In this
 * way we obtain ABI compatibility.
 *
 * Here we'll list all the structure versions in case they evolve over time,
 * however using a define, we'll make sure to use the last version as the
 * public name for the module to use. */

#define REDICTMODULE_CLIENTINFO_VERSION 1
typedef struct RedictModuleClientInfo {
    uint64_t version;       /* Version of this structure for ABI compat. */
    uint64_t flags;         /* REDICTMODULE_CLIENTINFO_FLAG_* */
    uint64_t id;            /* Client ID. */
    char addr[46];          /* IPv4 or IPv6 address. */
    uint16_t port;          /* TCP port. */
    uint16_t db;            /* Selected DB. */
} RedictModuleClientInfoV1;

#define RedictModuleClientInfo RedictModuleClientInfoV1

#define REDICTMODULE_CLIENTINFO_INITIALIZER_V1 { .version = 1 }

#define REDICTMODULE_REPLICATIONINFO_VERSION 1
typedef struct RedictModuleReplicationInfo {
    uint64_t version;       /* Not used since this structure is never passed
                               from the module to the core right now. Here
                               for future compatibility. */
    int master;             /* true if master, false if replica */
    char *masterhost;       /* master instance hostname for NOW_REPLICA */
    int masterport;         /* master instance port for NOW_REPLICA */
    char *replid1;          /* Main replication ID */
    char *replid2;          /* Secondary replication ID */
    uint64_t repl1_offset;  /* Main replication offset */
    uint64_t repl2_offset;  /* Offset of replid2 validity */
} RedictModuleReplicationInfoV1;

#define RedictModuleReplicationInfo RedictModuleReplicationInfoV1

#define REDICTMODULE_FLUSHINFO_VERSION 1
typedef struct RedictModuleFlushInfo {
    uint64_t version;       /* Not used since this structure is never passed
                               from the module to the core right now. Here
                               for future compatibility. */
    int32_t sync;           /* Synchronous or threaded flush?. */
    int32_t dbnum;          /* Flushed database number, -1 for ALL. */
} RedictModuleFlushInfoV1;

#define RedictModuleFlushInfo RedictModuleFlushInfoV1

#define REDICTMODULE_MODULE_CHANGE_VERSION 1
typedef struct RedictModuleModuleChange {
    uint64_t version;       /* Not used since this structure is never passed
                               from the module to the core right now. Here
                               for future compatibility. */
    const char* module_name;/* Name of module loaded or unloaded. */
    int32_t module_version; /* Module version. */
} RedictModuleModuleChangeV1;

#define RedictModuleModuleChange RedictModuleModuleChangeV1

#define REDICTMODULE_CONFIGCHANGE_VERSION 1
typedef struct RedictModuleConfigChange {
    uint64_t version;       /* Not used since this structure is never passed
                               from the module to the core right now. Here
                               for future compatibility. */
    uint32_t num_changes;   /* how many redict config options were changed */
    const char **config_names; /* the config names that were changed */
} RedictModuleConfigChangeV1;

#define RedictModuleConfigChange RedictModuleConfigChangeV1

#define REDICTMODULE_CRON_LOOP_VERSION 1
typedef struct RedictModuleCronLoopInfo {
    uint64_t version;       /* Not used since this structure is never passed
                               from the module to the core right now. Here
                               for future compatibility. */
    int32_t hz;             /* Approximate number of events per second. */
} RedictModuleCronLoopV1;

#define RedictModuleCronLoop RedictModuleCronLoopV1

#define REDICTMODULE_LOADING_PROGRESS_VERSION 1
typedef struct RedictModuleLoadingProgressInfo {
    uint64_t version;       /* Not used since this structure is never passed
                               from the module to the core right now. Here
                               for future compatibility. */
    int32_t hz;             /* Approximate number of events per second. */
    int32_t progress;       /* Approximate progress between 0 and 1024, or -1
                             * if unknown. */
} RedictModuleLoadingProgressV1;

#define RedictModuleLoadingProgress RedictModuleLoadingProgressV1

#define REDICTMODULE_SWAPDBINFO_VERSION 1
typedef struct RedictModuleSwapDbInfo {
    uint64_t version;       /* Not used since this structure is never passed
                               from the module to the core right now. Here
                               for future compatibility. */
    int32_t dbnum_first;    /* Swap Db first dbnum */
    int32_t dbnum_second;   /* Swap Db second dbnum */
} RedictModuleSwapDbInfoV1;

#define RedictModuleSwapDbInfo RedictModuleSwapDbInfoV1

#define REDICTMODULE_KEYINFO_VERSION 1
typedef struct RedictModuleKeyInfo {
    uint64_t version;       /* Not used since this structure is never passed
                               from the module to the core right now. Here
                               for future compatibility. */
    RedictModuleKey *key;    /* Opened key. */
} RedictModuleKeyInfoV1;

#define RedictModuleKeyInfo RedictModuleKeyInfoV1

typedef enum {
    REDICTMODULE_ACL_LOG_AUTH = 0, /* Authentication failure */
    REDICTMODULE_ACL_LOG_CMD, /* Command authorization failure */
    REDICTMODULE_ACL_LOG_KEY, /* Key authorization failure */
    REDICTMODULE_ACL_LOG_CHANNEL /* Channel authorization failure */
} RedictModuleACLLogEntryReason;

/* Incomplete structures needed by both the core and modules. */
typedef struct RedictModuleIO RedictModuleIO;
typedef struct RedictModuleDigest RedictModuleDigest;
typedef struct RedictModuleInfoCtx RedictModuleInfoCtx;
typedef struct RedictModuleDefragCtx RedictModuleDefragCtx;

/* Function pointers needed by both the core and modules, these needs to be
 * exposed since you can't cast a function pointer to (void *). */
typedef void (*RedictModuleInfoFunc)(RedictModuleInfoCtx *ctx, int for_crash_report);
typedef void (*RedictModuleDefragFunc)(RedictModuleDefragCtx *ctx);
typedef void (*RedictModuleUserChangedFunc) (uint64_t client_id, void *privdata);

/* ------------------------- End of common defines ------------------------ */

/* ----------- The rest of the defines are only for modules ----------------- */
#if !defined REDICTMODULE_CORE || defined REDICTMODULE_CORE_MODULE
/* Things defined for modules and core-modules. */

/* Macro definitions specific to individual compilers */
#ifndef REDICTMODULE_ATTR_UNUSED
#    ifdef __GNUC__
#        define REDICTMODULE_ATTR_UNUSED __attribute__((unused))
#    else
#        define REDICTMODULE_ATTR_UNUSED
#    endif
#endif

#ifndef REDICTMODULE_ATTR_PRINTF
#    ifdef __GNUC__
#        define REDICTMODULE_ATTR_PRINTF(idx,cnt) __attribute__((format(printf,idx,cnt)))
#    else
#        define REDICTMODULE_ATTR_PRINTF(idx,cnt)
#    endif
#endif

#ifndef REDICTMODULE_ATTR_COMMON
#    if defined(__GNUC__) && !(defined(__clang__) && defined(__cplusplus))
#        define REDICTMODULE_ATTR_COMMON __attribute__((__common__))
#    else
#        define REDICTMODULE_ATTR_COMMON
#    endif
#endif

/* Incomplete structures for compiler checks but opaque access. */
typedef struct RedictModuleCtx RedictModuleCtx;
typedef struct RedictModuleCommand RedictModuleCommand;
typedef struct RedictModuleCallReply RedictModuleCallReply;
typedef struct RedictModuleType RedictModuleType;
typedef struct RedictModuleBlockedClient RedictModuleBlockedClient;
typedef struct RedictModuleClusterInfo RedictModuleClusterInfo;
typedef struct RedictModuleDict RedictModuleDict;
typedef struct RedictModuleDictIter RedictModuleDictIter;
typedef struct RedictModuleCommandFilterCtx RedictModuleCommandFilterCtx;
typedef struct RedictModuleCommandFilter RedictModuleCommandFilter;
typedef struct RedictModuleServerInfoData RedictModuleServerInfoData;
typedef struct RedictModuleScanCursor RedictModuleScanCursor;
typedef struct RedictModuleUser RedictModuleUser;
typedef struct RedictModuleKeyOptCtx RedictModuleKeyOptCtx;
typedef struct RedictModuleRdbStream RedictModuleRdbStream;

typedef int (*RedictModuleCmdFunc)(RedictModuleCtx *ctx, RedictModuleString **argv, int argc);
typedef void (*RedictModuleDisconnectFunc)(RedictModuleCtx *ctx, RedictModuleBlockedClient *bc);
typedef int (*RedictModuleNotificationFunc)(RedictModuleCtx *ctx, int type, const char *event, RedictModuleString *key);
typedef void (*RedictModulePostNotificationJobFunc) (RedictModuleCtx *ctx, void *pd);
typedef void *(*RedictModuleTypeLoadFunc)(RedictModuleIO *rdb, int encver);
typedef void (*RedictModuleTypeSaveFunc)(RedictModuleIO *rdb, void *value);
typedef int (*RedictModuleTypeAuxLoadFunc)(RedictModuleIO *rdb, int encver, int when);
typedef void (*RedictModuleTypeAuxSaveFunc)(RedictModuleIO *rdb, int when);
typedef void (*RedictModuleTypeRewriteFunc)(RedictModuleIO *aof, RedictModuleString *key, void *value);
typedef size_t (*RedictModuleTypeMemUsageFunc)(const void *value);
typedef size_t (*RedictModuleTypeMemUsageFunc2)(RedictModuleKeyOptCtx *ctx, const void *value, size_t sample_size);
typedef void (*RedictModuleTypeDigestFunc)(RedictModuleDigest *digest, void *value);
typedef void (*RedictModuleTypeFreeFunc)(void *value);
typedef size_t (*RedictModuleTypeFreeEffortFunc)(RedictModuleString *key, const void *value);
typedef size_t (*RedictModuleTypeFreeEffortFunc2)(RedictModuleKeyOptCtx *ctx, const void *value);
typedef void (*RedictModuleTypeUnlinkFunc)(RedictModuleString *key, const void *value);
typedef void (*RedictModuleTypeUnlinkFunc2)(RedictModuleKeyOptCtx *ctx, const void *value);
typedef void *(*RedictModuleTypeCopyFunc)(RedictModuleString *fromkey, RedictModuleString *tokey, const void *value);
typedef void *(*RedictModuleTypeCopyFunc2)(RedictModuleKeyOptCtx *ctx, const void *value);
typedef int (*RedictModuleTypeDefragFunc)(RedictModuleDefragCtx *ctx, RedictModuleString *key, void **value);
typedef void (*RedictModuleClusterMessageReceiver)(RedictModuleCtx *ctx, const char *sender_id, uint8_t type, const unsigned char *payload, uint32_t len);
typedef void (*RedictModuleTimerProc)(RedictModuleCtx *ctx, void *data);
typedef void (*RedictModuleCommandFilterFunc) (RedictModuleCommandFilterCtx *filter);
typedef void (*RedictModuleForkDoneHandler) (int exitcode, int bysignal, void *user_data);
typedef void (*RedictModuleScanCB)(RedictModuleCtx *ctx, RedictModuleString *keyname, RedictModuleKey *key, void *privdata);
typedef void (*RedictModuleScanKeyCB)(RedictModuleKey *key, RedictModuleString *field, RedictModuleString *value, void *privdata);
typedef RedictModuleString * (*RedictModuleConfigGetStringFunc)(const char *name, void *privdata);
typedef long long (*RedictModuleConfigGetNumericFunc)(const char *name, void *privdata);
typedef int (*RedictModuleConfigGetBoolFunc)(const char *name, void *privdata);
typedef int (*RedictModuleConfigGetEnumFunc)(const char *name, void *privdata);
typedef int (*RedictModuleConfigSetStringFunc)(const char *name, RedictModuleString *val, void *privdata, RedictModuleString **err);
typedef int (*RedictModuleConfigSetNumericFunc)(const char *name, long long val, void *privdata, RedictModuleString **err);
typedef int (*RedictModuleConfigSetBoolFunc)(const char *name, int val, void *privdata, RedictModuleString **err);
typedef int (*RedictModuleConfigSetEnumFunc)(const char *name, int val, void *privdata, RedictModuleString **err);
typedef int (*RedictModuleConfigApplyFunc)(RedictModuleCtx *ctx, void *privdata, RedictModuleString **err);
typedef void (*RedictModuleOnUnblocked)(RedictModuleCtx *ctx, RedictModuleCallReply *reply, void *private_data);
typedef int (*RedictModuleAuthCallback)(RedictModuleCtx *ctx, RedictModuleString *username, RedictModuleString *password, RedictModuleString **err);

typedef struct RedictModuleTypeMethods {
    uint64_t version;
    RedictModuleTypeLoadFunc rdb_load;
    RedictModuleTypeSaveFunc rdb_save;
    RedictModuleTypeRewriteFunc aof_rewrite;
    RedictModuleTypeMemUsageFunc mem_usage;
    RedictModuleTypeDigestFunc digest;
    RedictModuleTypeFreeFunc free;
    RedictModuleTypeAuxLoadFunc aux_load;
    RedictModuleTypeAuxSaveFunc aux_save;
    int aux_save_triggers;
    RedictModuleTypeFreeEffortFunc free_effort;
    RedictModuleTypeUnlinkFunc unlink;
    RedictModuleTypeCopyFunc copy;
    RedictModuleTypeDefragFunc defrag;
    RedictModuleTypeMemUsageFunc2 mem_usage2;
    RedictModuleTypeFreeEffortFunc2 free_effort2;
    RedictModuleTypeUnlinkFunc2 unlink2;
    RedictModuleTypeCopyFunc2 copy2;
    RedictModuleTypeAuxSaveFunc aux_save2;
} RedictModuleTypeMethods;

#define REDICTMODULE_GET_API(name) \
    RedictModule_GetApi("RedictModule_" #name, ((void **)&RedictModule_ ## name))

/* Default API declaration prefix (not 'extern' for backwards compatibility) */
#ifndef REDICTMODULE_API
#define REDICTMODULE_API
#endif

/* Default API declaration suffix (compiler attributes) */
#ifndef REDICTMODULE_ATTR
#define REDICTMODULE_ATTR REDICTMODULE_ATTR_COMMON
#endif

REDICTMODULE_API void * (*RedictModule_Alloc)(size_t bytes) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_TryAlloc)(size_t bytes) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_Realloc)(void *ptr, size_t bytes) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_TryRealloc)(void *ptr, size_t bytes) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_Free)(void *ptr) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_Calloc)(size_t nmemb, size_t size) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_TryCalloc)(size_t nmemb, size_t size) REDICTMODULE_ATTR;
REDICTMODULE_API char * (*RedictModule_Strdup)(const char *str) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetApi)(const char *, void *) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_CreateCommand)(RedictModuleCtx *ctx, const char *name, RedictModuleCmdFunc cmdfunc, const char *strflags, int firstkey, int lastkey, int keystep) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleCommand *(*RedictModule_GetCommand)(RedictModuleCtx *ctx, const char *name) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_CreateSubcommand)(RedictModuleCommand *parent, const char *name, RedictModuleCmdFunc cmdfunc, const char *strflags, int firstkey, int lastkey, int keystep) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_SetCommandInfo)(RedictModuleCommand *command, const RedictModuleCommandInfo *info) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_SetCommandACLCategories)(RedictModuleCommand *command, const char *ctgrsflags) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_AddACLCategory)(RedictModuleCtx *ctx, const char *name) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_SetModuleAttribs)(RedictModuleCtx *ctx, const char *name, int ver, int apiver) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_IsModuleNameBusy)(const char *name) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_WrongArity)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithLongLong)(RedictModuleCtx *ctx, long long ll) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetSelectedDb)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_SelectDb)(RedictModuleCtx *ctx, int newid) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_KeyExists)(RedictModuleCtx *ctx, RedictModuleString *keyname) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleKey * (*RedictModule_OpenKey)(RedictModuleCtx *ctx, RedictModuleString *keyname, int mode) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetOpenKeyModesAll)(void) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_CloseKey)(RedictModuleKey *kp) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_KeyType)(RedictModuleKey *kp) REDICTMODULE_ATTR;
REDICTMODULE_API size_t (*RedictModule_ValueLength)(RedictModuleKey *kp) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ListPush)(RedictModuleKey *kp, int where, RedictModuleString *ele) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_ListPop)(RedictModuleKey *key, int where) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_ListGet)(RedictModuleKey *key, long index) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ListSet)(RedictModuleKey *key, long index, RedictModuleString *value) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ListInsert)(RedictModuleKey *key, long index, RedictModuleString *value) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ListDelete)(RedictModuleKey *key, long index) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleCallReply * (*RedictModule_Call)(RedictModuleCtx *ctx, const char *cmdname, const char *fmt, ...) REDICTMODULE_ATTR;
REDICTMODULE_API const char * (*RedictModule_CallReplyProto)(RedictModuleCallReply *reply, size_t *len) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_FreeCallReply)(RedictModuleCallReply *reply) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_CallReplyType)(RedictModuleCallReply *reply) REDICTMODULE_ATTR;
REDICTMODULE_API long long (*RedictModule_CallReplyInteger)(RedictModuleCallReply *reply) REDICTMODULE_ATTR;
REDICTMODULE_API double (*RedictModule_CallReplyDouble)(RedictModuleCallReply *reply) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_CallReplyBool)(RedictModuleCallReply *reply) REDICTMODULE_ATTR;
REDICTMODULE_API const char* (*RedictModule_CallReplyBigNumber)(RedictModuleCallReply *reply, size_t *len) REDICTMODULE_ATTR;
REDICTMODULE_API const char* (*RedictModule_CallReplyVerbatim)(RedictModuleCallReply *reply, size_t *len, const char **format) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleCallReply * (*RedictModule_CallReplySetElement)(RedictModuleCallReply *reply, size_t idx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_CallReplyMapElement)(RedictModuleCallReply *reply, size_t idx, RedictModuleCallReply **key, RedictModuleCallReply **val) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_CallReplyAttributeElement)(RedictModuleCallReply *reply, size_t idx, RedictModuleCallReply **key, RedictModuleCallReply **val) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_CallReplyPromiseSetUnblockHandler)(RedictModuleCallReply *reply, RedictModuleOnUnblocked on_unblock, void *private_data) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_CallReplyPromiseAbort)(RedictModuleCallReply *reply, void **private_data) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleCallReply * (*RedictModule_CallReplyAttribute)(RedictModuleCallReply *reply) REDICTMODULE_ATTR;
REDICTMODULE_API size_t (*RedictModule_CallReplyLength)(RedictModuleCallReply *reply) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleCallReply * (*RedictModule_CallReplyArrayElement)(RedictModuleCallReply *reply, size_t idx) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_CreateString)(RedictModuleCtx *ctx, const char *ptr, size_t len) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_CreateStringFromLongLong)(RedictModuleCtx *ctx, long long ll) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_CreateStringFromULongLong)(RedictModuleCtx *ctx, unsigned long long ull) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_CreateStringFromDouble)(RedictModuleCtx *ctx, double d) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_CreateStringFromLongDouble)(RedictModuleCtx *ctx, long double ld, int humanfriendly) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_CreateStringFromString)(RedictModuleCtx *ctx, const RedictModuleString *str) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_CreateStringFromStreamID)(RedictModuleCtx *ctx, const RedictModuleStreamID *id) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_CreateStringPrintf)(RedictModuleCtx *ctx, const char *fmt, ...) REDICTMODULE_ATTR_PRINTF(2,3) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_FreeString)(RedictModuleCtx *ctx, RedictModuleString *str) REDICTMODULE_ATTR;
REDICTMODULE_API const char * (*RedictModule_StringPtrLen)(const RedictModuleString *str, size_t *len) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithError)(RedictModuleCtx *ctx, const char *err) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithErrorFormat)(RedictModuleCtx *ctx, const char *fmt, ...) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithSimpleString)(RedictModuleCtx *ctx, const char *msg) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithArray)(RedictModuleCtx *ctx, long len) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithMap)(RedictModuleCtx *ctx, long len) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithSet)(RedictModuleCtx *ctx, long len) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithAttribute)(RedictModuleCtx *ctx, long len) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithNullArray)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithEmptyArray)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_ReplySetArrayLength)(RedictModuleCtx *ctx, long len) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_ReplySetMapLength)(RedictModuleCtx *ctx, long len) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_ReplySetSetLength)(RedictModuleCtx *ctx, long len) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_ReplySetAttributeLength)(RedictModuleCtx *ctx, long len) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_ReplySetPushLength)(RedictModuleCtx *ctx, long len) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithStringBuffer)(RedictModuleCtx *ctx, const char *buf, size_t len) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithCString)(RedictModuleCtx *ctx, const char *buf) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithString)(RedictModuleCtx *ctx, RedictModuleString *str) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithEmptyString)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithVerbatimString)(RedictModuleCtx *ctx, const char *buf, size_t len) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithVerbatimStringType)(RedictModuleCtx *ctx, const char *buf, size_t len, const char *ext) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithNull)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithBool)(RedictModuleCtx *ctx, int b) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithLongDouble)(RedictModuleCtx *ctx, long double d) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithDouble)(RedictModuleCtx *ctx, double d) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithBigNumber)(RedictModuleCtx *ctx, const char *bignum, size_t len) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplyWithCallReply)(RedictModuleCtx *ctx, RedictModuleCallReply *reply) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StringToLongLong)(const RedictModuleString *str, long long *ll) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StringToULongLong)(const RedictModuleString *str, unsigned long long *ull) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StringToDouble)(const RedictModuleString *str, double *d) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StringToLongDouble)(const RedictModuleString *str, long double *d) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StringToStreamID)(const RedictModuleString *str, RedictModuleStreamID *id) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_AutoMemory)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_Replicate)(RedictModuleCtx *ctx, const char *cmdname, const char *fmt, ...) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ReplicateVerbatim)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API const char * (*RedictModule_CallReplyStringPtr)(RedictModuleCallReply *reply, size_t *len) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_CreateStringFromCallReply)(RedictModuleCallReply *reply) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DeleteKey)(RedictModuleKey *key) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_UnlinkKey)(RedictModuleKey *key) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StringSet)(RedictModuleKey *key, RedictModuleString *str) REDICTMODULE_ATTR;
REDICTMODULE_API char * (*RedictModule_StringDMA)(RedictModuleKey *key, size_t *len, int mode) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StringTruncate)(RedictModuleKey *key, size_t newlen) REDICTMODULE_ATTR;
REDICTMODULE_API mstime_t (*RedictModule_GetExpire)(RedictModuleKey *key) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_SetExpire)(RedictModuleKey *key, mstime_t expire) REDICTMODULE_ATTR;
REDICTMODULE_API mstime_t (*RedictModule_GetAbsExpire)(RedictModuleKey *key) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_SetAbsExpire)(RedictModuleKey *key, mstime_t expire) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_ResetDataset)(int restart_aof, int async) REDICTMODULE_ATTR;
REDICTMODULE_API unsigned long long (*RedictModule_DbSize)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_RandomKey)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ZsetAdd)(RedictModuleKey *key, double score, RedictModuleString *ele, int *flagsptr) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ZsetIncrby)(RedictModuleKey *key, double score, RedictModuleString *ele, int *flagsptr, double *newscore) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ZsetScore)(RedictModuleKey *key, RedictModuleString *ele, double *score) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ZsetRem)(RedictModuleKey *key, RedictModuleString *ele, int *deleted) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_ZsetRangeStop)(RedictModuleKey *key) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ZsetFirstInScoreRange)(RedictModuleKey *key, double min, double max, int minex, int maxex) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ZsetLastInScoreRange)(RedictModuleKey *key, double min, double max, int minex, int maxex) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ZsetFirstInLexRange)(RedictModuleKey *key, RedictModuleString *min, RedictModuleString *max) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ZsetLastInLexRange)(RedictModuleKey *key, RedictModuleString *min, RedictModuleString *max) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_ZsetRangeCurrentElement)(RedictModuleKey *key, double *score) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ZsetRangeNext)(RedictModuleKey *key) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ZsetRangePrev)(RedictModuleKey *key) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ZsetRangeEndReached)(RedictModuleKey *key) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_HashSet)(RedictModuleKey *key, int flags, ...) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_HashGet)(RedictModuleKey *key, int flags, ...) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StreamAdd)(RedictModuleKey *key, int flags, RedictModuleStreamID *id, RedictModuleString **argv, int64_t numfields) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StreamDelete)(RedictModuleKey *key, RedictModuleStreamID *id) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StreamIteratorStart)(RedictModuleKey *key, int flags, RedictModuleStreamID *startid, RedictModuleStreamID *endid) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StreamIteratorStop)(RedictModuleKey *key) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StreamIteratorNextID)(RedictModuleKey *key, RedictModuleStreamID *id, long *numfields) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StreamIteratorNextField)(RedictModuleKey *key, RedictModuleString **field_ptr, RedictModuleString **value_ptr) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StreamIteratorDelete)(RedictModuleKey *key) REDICTMODULE_ATTR;
REDICTMODULE_API long long (*RedictModule_StreamTrimByLength)(RedictModuleKey *key, int flags, long long length) REDICTMODULE_ATTR;
REDICTMODULE_API long long (*RedictModule_StreamTrimByID)(RedictModuleKey *key, int flags, RedictModuleStreamID *id) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_IsKeysPositionRequest)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_KeyAtPos)(RedictModuleCtx *ctx, int pos) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_KeyAtPosWithFlags)(RedictModuleCtx *ctx, int pos, int flags) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_IsChannelsPositionRequest)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_ChannelAtPosWithFlags)(RedictModuleCtx *ctx, int pos, int flags) REDICTMODULE_ATTR;
REDICTMODULE_API unsigned long long (*RedictModule_GetClientId)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_GetClientUserNameById)(RedictModuleCtx *ctx, uint64_t id) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetClientInfoById)(void *ci, uint64_t id) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_GetClientNameById)(RedictModuleCtx *ctx, uint64_t id) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_SetClientNameById)(uint64_t id, RedictModuleString *name) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_PublishMessage)(RedictModuleCtx *ctx, RedictModuleString *channel, RedictModuleString *message) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_PublishMessageShard)(RedictModuleCtx *ctx, RedictModuleString *channel, RedictModuleString *message) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetContextFlags)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_AvoidReplicaTraffic)(void) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_PoolAlloc)(RedictModuleCtx *ctx, size_t bytes) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleType * (*RedictModule_CreateDataType)(RedictModuleCtx *ctx, const char *name, int encver, RedictModuleTypeMethods *typemethods) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ModuleTypeSetValue)(RedictModuleKey *key, RedictModuleType *mt, void *value) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ModuleTypeReplaceValue)(RedictModuleKey *key, RedictModuleType *mt, void *new_value, void **old_value) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleType * (*RedictModule_ModuleTypeGetType)(RedictModuleKey *key) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_ModuleTypeGetValue)(RedictModuleKey *key) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_IsIOError)(RedictModuleIO *io) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_SetModuleOptions)(RedictModuleCtx *ctx, int options) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_SignalModifiedKey)(RedictModuleCtx *ctx, RedictModuleString *keyname) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_SaveUnsigned)(RedictModuleIO *io, uint64_t value) REDICTMODULE_ATTR;
REDICTMODULE_API uint64_t (*RedictModule_LoadUnsigned)(RedictModuleIO *io) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_SaveSigned)(RedictModuleIO *io, int64_t value) REDICTMODULE_ATTR;
REDICTMODULE_API int64_t (*RedictModule_LoadSigned)(RedictModuleIO *io) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_EmitAOF)(RedictModuleIO *io, const char *cmdname, const char *fmt, ...) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_SaveString)(RedictModuleIO *io, RedictModuleString *s) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_SaveStringBuffer)(RedictModuleIO *io, const char *str, size_t len) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_LoadString)(RedictModuleIO *io) REDICTMODULE_ATTR;
REDICTMODULE_API char * (*RedictModule_LoadStringBuffer)(RedictModuleIO *io, size_t *lenptr) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_SaveDouble)(RedictModuleIO *io, double value) REDICTMODULE_ATTR;
REDICTMODULE_API double (*RedictModule_LoadDouble)(RedictModuleIO *io) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_SaveFloat)(RedictModuleIO *io, float value) REDICTMODULE_ATTR;
REDICTMODULE_API float (*RedictModule_LoadFloat)(RedictModuleIO *io) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_SaveLongDouble)(RedictModuleIO *io, long double value) REDICTMODULE_ATTR;
REDICTMODULE_API long double (*RedictModule_LoadLongDouble)(RedictModuleIO *io) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_LoadDataTypeFromString)(const RedictModuleString *str, const RedictModuleType *mt) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_LoadDataTypeFromStringEncver)(const RedictModuleString *str, const RedictModuleType *mt, int encver) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_SaveDataTypeToString)(RedictModuleCtx *ctx, void *data, const RedictModuleType *mt) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_Log)(RedictModuleCtx *ctx, const char *level, const char *fmt, ...) REDICTMODULE_ATTR REDICTMODULE_ATTR_PRINTF(3,4);
REDICTMODULE_API void (*RedictModule_LogIOError)(RedictModuleIO *io, const char *levelstr, const char *fmt, ...) REDICTMODULE_ATTR REDICTMODULE_ATTR_PRINTF(3,4);
REDICTMODULE_API void (*RedictModule__Assert)(const char *estr, const char *file, int line) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_LatencyAddSample)(const char *event, mstime_t latency) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StringAppendBuffer)(RedictModuleCtx *ctx, RedictModuleString *str, const char *buf, size_t len) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_TrimStringAllocation)(RedictModuleString *str) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_RetainString)(RedictModuleCtx *ctx, RedictModuleString *str) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_HoldString)(RedictModuleCtx *ctx, RedictModuleString *str) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StringCompare)(const RedictModuleString *a, const RedictModuleString *b) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleCtx * (*RedictModule_GetContextFromIO)(RedictModuleIO *io) REDICTMODULE_ATTR;
REDICTMODULE_API const RedictModuleString * (*RedictModule_GetKeyNameFromIO)(RedictModuleIO *io) REDICTMODULE_ATTR;
REDICTMODULE_API const RedictModuleString * (*RedictModule_GetKeyNameFromModuleKey)(RedictModuleKey *key) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetDbIdFromModuleKey)(RedictModuleKey *key) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetDbIdFromIO)(RedictModuleIO *io) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetDbIdFromOptCtx)(RedictModuleKeyOptCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetToDbIdFromOptCtx)(RedictModuleKeyOptCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API const RedictModuleString * (*RedictModule_GetKeyNameFromOptCtx)(RedictModuleKeyOptCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API const RedictModuleString * (*RedictModule_GetToKeyNameFromOptCtx)(RedictModuleKeyOptCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API mstime_t (*RedictModule_Milliseconds)(void) REDICTMODULE_ATTR;
REDICTMODULE_API uint64_t (*RedictModule_MonotonicMicroseconds)(void) REDICTMODULE_ATTR;
REDICTMODULE_API ustime_t (*RedictModule_Microseconds)(void) REDICTMODULE_ATTR;
REDICTMODULE_API ustime_t (*RedictModule_CachedMicroseconds)(void) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_DigestAddStringBuffer)(RedictModuleDigest *md, const char *ele, size_t len) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_DigestAddLongLong)(RedictModuleDigest *md, long long ele) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_DigestEndSequence)(RedictModuleDigest *md) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetDbIdFromDigest)(RedictModuleDigest *dig) REDICTMODULE_ATTR;
REDICTMODULE_API const RedictModuleString * (*RedictModule_GetKeyNameFromDigest)(RedictModuleDigest *dig) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleDict * (*RedictModule_CreateDict)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_FreeDict)(RedictModuleCtx *ctx, RedictModuleDict *d) REDICTMODULE_ATTR;
REDICTMODULE_API uint64_t (*RedictModule_DictSize)(RedictModuleDict *d) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DictSetC)(RedictModuleDict *d, void *key, size_t keylen, void *ptr) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DictReplaceC)(RedictModuleDict *d, void *key, size_t keylen, void *ptr) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DictSet)(RedictModuleDict *d, RedictModuleString *key, void *ptr) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DictReplace)(RedictModuleDict *d, RedictModuleString *key, void *ptr) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_DictGetC)(RedictModuleDict *d, void *key, size_t keylen, int *nokey) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_DictGet)(RedictModuleDict *d, RedictModuleString *key, int *nokey) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DictDelC)(RedictModuleDict *d, void *key, size_t keylen, void *oldval) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DictDel)(RedictModuleDict *d, RedictModuleString *key, void *oldval) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleDictIter * (*RedictModule_DictIteratorStartC)(RedictModuleDict *d, const char *op, void *key, size_t keylen) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleDictIter * (*RedictModule_DictIteratorStart)(RedictModuleDict *d, const char *op, RedictModuleString *key) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_DictIteratorStop)(RedictModuleDictIter *di) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DictIteratorReseekC)(RedictModuleDictIter *di, const char *op, void *key, size_t keylen) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DictIteratorReseek)(RedictModuleDictIter *di, const char *op, RedictModuleString *key) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_DictNextC)(RedictModuleDictIter *di, size_t *keylen, void **dataptr) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_DictPrevC)(RedictModuleDictIter *di, size_t *keylen, void **dataptr) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_DictNext)(RedictModuleCtx *ctx, RedictModuleDictIter *di, void **dataptr) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_DictPrev)(RedictModuleCtx *ctx, RedictModuleDictIter *di, void **dataptr) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DictCompareC)(RedictModuleDictIter *di, const char *op, void *key, size_t keylen) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DictCompare)(RedictModuleDictIter *di, const char *op, RedictModuleString *key) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_RegisterInfoFunc)(RedictModuleCtx *ctx, RedictModuleInfoFunc cb) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_RegisterAuthCallback)(RedictModuleCtx *ctx, RedictModuleAuthCallback cb) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_InfoAddSection)(RedictModuleInfoCtx *ctx, const char *name) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_InfoBeginDictField)(RedictModuleInfoCtx *ctx, const char *name) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_InfoEndDictField)(RedictModuleInfoCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_InfoAddFieldString)(RedictModuleInfoCtx *ctx, const char *field, RedictModuleString *value) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_InfoAddFieldCString)(RedictModuleInfoCtx *ctx, const char *field,const  char *value) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_InfoAddFieldDouble)(RedictModuleInfoCtx *ctx, const char *field, double value) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_InfoAddFieldLongLong)(RedictModuleInfoCtx *ctx, const char *field, long long value) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_InfoAddFieldULongLong)(RedictModuleInfoCtx *ctx, const char *field, unsigned long long value) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleServerInfoData * (*RedictModule_GetServerInfo)(RedictModuleCtx *ctx, const char *section) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_FreeServerInfo)(RedictModuleCtx *ctx, RedictModuleServerInfoData *data) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_ServerInfoGetField)(RedictModuleCtx *ctx, RedictModuleServerInfoData *data, const char* field) REDICTMODULE_ATTR;
REDICTMODULE_API const char * (*RedictModule_ServerInfoGetFieldC)(RedictModuleServerInfoData *data, const char* field) REDICTMODULE_ATTR;
REDICTMODULE_API long long (*RedictModule_ServerInfoGetFieldSigned)(RedictModuleServerInfoData *data, const char* field, int *out_err) REDICTMODULE_ATTR;
REDICTMODULE_API unsigned long long (*RedictModule_ServerInfoGetFieldUnsigned)(RedictModuleServerInfoData *data, const char* field, int *out_err) REDICTMODULE_ATTR;
REDICTMODULE_API double (*RedictModule_ServerInfoGetFieldDouble)(RedictModuleServerInfoData *data, const char* field, int *out_err) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_SubscribeToServerEvent)(RedictModuleCtx *ctx, RedictModuleEvent event, RedictModuleEventCallback callback) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_SetLRU)(RedictModuleKey *key, mstime_t lru_idle) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetLRU)(RedictModuleKey *key, mstime_t *lru_idle) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_SetLFU)(RedictModuleKey *key, long long lfu_freq) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetLFU)(RedictModuleKey *key, long long *lfu_freq) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleBlockedClient * (*RedictModule_BlockClientOnKeys)(RedictModuleCtx *ctx, RedictModuleCmdFunc reply_callback, RedictModuleCmdFunc timeout_callback, void (*free_privdata)(RedictModuleCtx*,void*), long long timeout_ms, RedictModuleString **keys, int numkeys, void *privdata) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleBlockedClient * (*RedictModule_BlockClientOnKeysWithFlags)(RedictModuleCtx *ctx, RedictModuleCmdFunc reply_callback, RedictModuleCmdFunc timeout_callback, void (*free_privdata)(RedictModuleCtx*,void*), long long timeout_ms, RedictModuleString **keys, int numkeys, void *privdata, int flags) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_SignalKeyAsReady)(RedictModuleCtx *ctx, RedictModuleString *key) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_GetBlockedClientReadyKey)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleScanCursor * (*RedictModule_ScanCursorCreate)(void) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_ScanCursorRestart)(RedictModuleScanCursor *cursor) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_ScanCursorDestroy)(RedictModuleScanCursor *cursor) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_Scan)(RedictModuleCtx *ctx, RedictModuleScanCursor *cursor, RedictModuleScanCB fn, void *privdata) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ScanKey)(RedictModuleKey *key, RedictModuleScanCursor *cursor, RedictModuleScanKeyCB fn, void *privdata) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetContextFlagsAll)(void) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetModuleOptionsAll)(void) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetKeyspaceNotificationFlagsAll)(void) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_IsSubEventSupported)(RedictModuleEvent event, uint64_t subevent) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetServerVersion)(void) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetTypeMethodVersion)(void) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_Yield)(RedictModuleCtx *ctx, int flags, const char *busy_reply) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleBlockedClient * (*RedictModule_BlockClient)(RedictModuleCtx *ctx, RedictModuleCmdFunc reply_callback, RedictModuleCmdFunc timeout_callback, void (*free_privdata)(RedictModuleCtx*,void*), long long timeout_ms) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_BlockClientGetPrivateData)(RedictModuleBlockedClient *blocked_client) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_BlockClientSetPrivateData)(RedictModuleBlockedClient *blocked_client, void *private_data) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleBlockedClient * (*RedictModule_BlockClientOnAuth)(RedictModuleCtx *ctx, RedictModuleAuthCallback reply_callback, void (*free_privdata)(RedictModuleCtx*,void*)) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_UnblockClient)(RedictModuleBlockedClient *bc, void *privdata) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_IsBlockedReplyRequest)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_IsBlockedTimeoutRequest)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_GetBlockedClientPrivateData)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleBlockedClient * (*RedictModule_GetBlockedClientHandle)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_AbortBlock)(RedictModuleBlockedClient *bc) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_BlockedClientMeasureTimeStart)(RedictModuleBlockedClient *bc) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_BlockedClientMeasureTimeEnd)(RedictModuleBlockedClient *bc) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleCtx * (*RedictModule_GetThreadSafeContext)(RedictModuleBlockedClient *bc) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleCtx * (*RedictModule_GetDetachedThreadSafeContext)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_FreeThreadSafeContext)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_ThreadSafeContextLock)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ThreadSafeContextTryLock)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_ThreadSafeContextUnlock)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_SubscribeToKeyspaceEvents)(RedictModuleCtx *ctx, int types, RedictModuleNotificationFunc cb) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_AddPostNotificationJob)(RedictModuleCtx *ctx, RedictModulePostNotificationJobFunc callback, void *pd, void (*free_pd)(void*)) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_NotifyKeyspaceEvent)(RedictModuleCtx *ctx, int type, const char *event, RedictModuleString *key) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetNotifyKeyspaceEvents)(void) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_BlockedClientDisconnected)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_RegisterClusterMessageReceiver)(RedictModuleCtx *ctx, uint8_t type, RedictModuleClusterMessageReceiver callback) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_SendClusterMessage)(RedictModuleCtx *ctx, const char *target_id, uint8_t type, const char *msg, uint32_t len) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetClusterNodeInfo)(RedictModuleCtx *ctx, const char *id, char *ip, char *master_id, int *port, int *flags) REDICTMODULE_ATTR;
REDICTMODULE_API char ** (*RedictModule_GetClusterNodesList)(RedictModuleCtx *ctx, size_t *numnodes) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_FreeClusterNodesList)(char **ids) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleTimerID (*RedictModule_CreateTimer)(RedictModuleCtx *ctx, mstime_t period, RedictModuleTimerProc callback, void *data) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_StopTimer)(RedictModuleCtx *ctx, RedictModuleTimerID id, void **data) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetTimerInfo)(RedictModuleCtx *ctx, RedictModuleTimerID id, uint64_t *remaining, void **data) REDICTMODULE_ATTR;
REDICTMODULE_API const char * (*RedictModule_GetMyClusterID)(void) REDICTMODULE_ATTR;
REDICTMODULE_API size_t (*RedictModule_GetClusterSize)(void) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_GetRandomBytes)(unsigned char *dst, size_t len) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_GetRandomHexChars)(char *dst, size_t len) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_SetDisconnectCallback)(RedictModuleBlockedClient *bc, RedictModuleDisconnectFunc callback) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_SetClusterFlags)(RedictModuleCtx *ctx, uint64_t flags) REDICTMODULE_ATTR;
REDICTMODULE_API unsigned int (*RedictModule_ClusterKeySlot)(RedictModuleString *key) REDICTMODULE_ATTR;
REDICTMODULE_API const char *(*RedictModule_ClusterCanonicalKeyNameInSlot)(unsigned int slot) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ExportSharedAPI)(RedictModuleCtx *ctx, const char *apiname, void *func) REDICTMODULE_ATTR;
REDICTMODULE_API void * (*RedictModule_GetSharedAPI)(RedictModuleCtx *ctx, const char *apiname) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleCommandFilter * (*RedictModule_RegisterCommandFilter)(RedictModuleCtx *ctx, RedictModuleCommandFilterFunc cb, int flags) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_UnregisterCommandFilter)(RedictModuleCtx *ctx, RedictModuleCommandFilter *filter) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_CommandFilterArgsCount)(RedictModuleCommandFilterCtx *fctx) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_CommandFilterArgGet)(RedictModuleCommandFilterCtx *fctx, int pos) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_CommandFilterArgInsert)(RedictModuleCommandFilterCtx *fctx, int pos, RedictModuleString *arg) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_CommandFilterArgReplace)(RedictModuleCommandFilterCtx *fctx, int pos, RedictModuleString *arg) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_CommandFilterArgDelete)(RedictModuleCommandFilterCtx *fctx, int pos) REDICTMODULE_ATTR;
REDICTMODULE_API unsigned long long (*RedictModule_CommandFilterGetClientId)(RedictModuleCommandFilterCtx *fctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_Fork)(RedictModuleForkDoneHandler cb, void *user_data) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_SendChildHeartbeat)(double progress) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ExitFromChild)(int retcode) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_KillForkChild)(int child_pid) REDICTMODULE_ATTR;
REDICTMODULE_API float (*RedictModule_GetUsedMemoryRatio)(void) REDICTMODULE_ATTR;
REDICTMODULE_API size_t (*RedictModule_MallocSize)(void* ptr) REDICTMODULE_ATTR;
REDICTMODULE_API size_t (*RedictModule_MallocUsableSize)(void *ptr) REDICTMODULE_ATTR;
REDICTMODULE_API size_t (*RedictModule_MallocSizeString)(RedictModuleString* str) REDICTMODULE_ATTR;
REDICTMODULE_API size_t (*RedictModule_MallocSizeDict)(RedictModuleDict* dict) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleUser * (*RedictModule_CreateModuleUser)(const char *name) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_FreeModuleUser)(RedictModuleUser *user) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_SetContextUser)(RedictModuleCtx *ctx, const RedictModuleUser *user) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_SetModuleUserACL)(RedictModuleUser *user, const char* acl) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_SetModuleUserACLString)(RedictModuleCtx * ctx, RedictModuleUser *user, const char* acl, RedictModuleString **error) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_GetModuleUserACLString)(RedictModuleUser *user) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_GetCurrentUserName)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleUser * (*RedictModule_GetModuleUserFromUserName)(RedictModuleString *name) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ACLCheckCommandPermissions)(RedictModuleUser *user, RedictModuleString **argv, int argc) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ACLCheckKeyPermissions)(RedictModuleUser *user, RedictModuleString *key, int flags) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_ACLCheckChannelPermissions)(RedictModuleUser *user, RedictModuleString *ch, int literal) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_ACLAddLogEntry)(RedictModuleCtx *ctx, RedictModuleUser *user, RedictModuleString *object, RedictModuleACLLogEntryReason reason) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_ACLAddLogEntryByUserName)(RedictModuleCtx *ctx, RedictModuleString *user, RedictModuleString *object, RedictModuleACLLogEntryReason reason) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_AuthenticateClientWithACLUser)(RedictModuleCtx *ctx, const char *name, size_t len, RedictModuleUserChangedFunc callback, void *privdata, uint64_t *client_id) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_AuthenticateClientWithUser)(RedictModuleCtx *ctx, RedictModuleUser *user, RedictModuleUserChangedFunc callback, void *privdata, uint64_t *client_id) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DeauthenticateAndCloseClient)(RedictModuleCtx *ctx, uint64_t client_id) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_RedactClientCommandArgument)(RedictModuleCtx *ctx, int pos) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString * (*RedictModule_GetClientCertificate)(RedictModuleCtx *ctx, uint64_t id) REDICTMODULE_ATTR;
REDICTMODULE_API int *(*RedictModule_GetCommandKeys)(RedictModuleCtx *ctx, RedictModuleString **argv, int argc, int *num_keys) REDICTMODULE_ATTR;
REDICTMODULE_API int *(*RedictModule_GetCommandKeysWithFlags)(RedictModuleCtx *ctx, RedictModuleString **argv, int argc, int *num_keys, int **out_flags) REDICTMODULE_ATTR;
REDICTMODULE_API const char *(*RedictModule_GetCurrentCommandName)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_RegisterDefragFunc)(RedictModuleCtx *ctx, RedictModuleDefragFunc func) REDICTMODULE_ATTR;
REDICTMODULE_API void *(*RedictModule_DefragAlloc)(RedictModuleDefragCtx *ctx, void *ptr) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleString *(*RedictModule_DefragRedictModuleString)(RedictModuleDefragCtx *ctx, RedictModuleString *str) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DefragShouldStop)(RedictModuleDefragCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DefragCursorSet)(RedictModuleDefragCtx *ctx, unsigned long cursor) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_DefragCursorGet)(RedictModuleDefragCtx *ctx, unsigned long *cursor) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_GetDbIdFromDefragCtx)(RedictModuleDefragCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API const RedictModuleString * (*RedictModule_GetKeyNameFromDefragCtx)(RedictModuleDefragCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_EventLoopAdd)(int fd, int mask, RedictModuleEventLoopFunc func, void *user_data) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_EventLoopDel)(int fd, int mask) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_EventLoopAddOneShot)(RedictModuleEventLoopOneShotFunc func, void *user_data) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_RegisterBoolConfig)(RedictModuleCtx *ctx, const char *name, int default_val, unsigned int flags, RedictModuleConfigGetBoolFunc getfn, RedictModuleConfigSetBoolFunc setfn, RedictModuleConfigApplyFunc applyfn, void *privdata) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_RegisterNumericConfig)(RedictModuleCtx *ctx, const char *name, long long default_val, unsigned int flags, long long min, long long max, RedictModuleConfigGetNumericFunc getfn, RedictModuleConfigSetNumericFunc setfn, RedictModuleConfigApplyFunc applyfn, void *privdata) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_RegisterStringConfig)(RedictModuleCtx *ctx, const char *name, const char *default_val, unsigned int flags, RedictModuleConfigGetStringFunc getfn, RedictModuleConfigSetStringFunc setfn, RedictModuleConfigApplyFunc applyfn, void *privdata) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_RegisterEnumConfig)(RedictModuleCtx *ctx, const char *name, int default_val, unsigned int flags, const char **enum_values, const int *int_values, int num_enum_vals, RedictModuleConfigGetEnumFunc getfn, RedictModuleConfigSetEnumFunc setfn, RedictModuleConfigApplyFunc applyfn, void *privdata) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_LoadConfigs)(RedictModuleCtx *ctx) REDICTMODULE_ATTR;
REDICTMODULE_API RedictModuleRdbStream *(*RedictModule_RdbStreamCreateFromFile)(const char *filename) REDICTMODULE_ATTR;
REDICTMODULE_API void (*RedictModule_RdbStreamFree)(RedictModuleRdbStream *stream) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_RdbLoad)(RedictModuleCtx *ctx, RedictModuleRdbStream *stream, int flags) REDICTMODULE_ATTR;
REDICTMODULE_API int (*RedictModule_RdbSave)(RedictModuleCtx *ctx, RedictModuleRdbStream *stream, int flags) REDICTMODULE_ATTR;

#define RedictModule_IsAOFClient(id) ((id) == UINT64_MAX)

/* This is included inline inside each Redict module. */
static int RedictModule_Init(RedictModuleCtx *ctx, const char *name, int ver, int apiver) REDICTMODULE_ATTR_UNUSED;
static int RedictModule_Init(RedictModuleCtx *ctx, const char *name, int ver, int apiver) {
    void *getapifuncptr = ((void**)ctx)[0];
    RedictModule_GetApi = (int (*)(const char *, void *)) (unsigned long)getapifuncptr;
    REDICTMODULE_GET_API(Alloc);
    REDICTMODULE_GET_API(TryAlloc);
    REDICTMODULE_GET_API(Calloc);
    REDICTMODULE_GET_API(TryCalloc);
    REDICTMODULE_GET_API(Free);
    REDICTMODULE_GET_API(Realloc);
    REDICTMODULE_GET_API(TryRealloc);
    REDICTMODULE_GET_API(Strdup);
    REDICTMODULE_GET_API(CreateCommand);
    REDICTMODULE_GET_API(GetCommand);
    REDICTMODULE_GET_API(CreateSubcommand);
    REDICTMODULE_GET_API(SetCommandInfo);
    REDICTMODULE_GET_API(SetCommandACLCategories);
    REDICTMODULE_GET_API(AddACLCategory);
    REDICTMODULE_GET_API(SetModuleAttribs);
    REDICTMODULE_GET_API(IsModuleNameBusy);
    REDICTMODULE_GET_API(WrongArity);
    REDICTMODULE_GET_API(ReplyWithLongLong);
    REDICTMODULE_GET_API(ReplyWithError);
    REDICTMODULE_GET_API(ReplyWithErrorFormat);
    REDICTMODULE_GET_API(ReplyWithSimpleString);
    REDICTMODULE_GET_API(ReplyWithArray);
    REDICTMODULE_GET_API(ReplyWithMap);
    REDICTMODULE_GET_API(ReplyWithSet);
    REDICTMODULE_GET_API(ReplyWithAttribute);
    REDICTMODULE_GET_API(ReplyWithNullArray);
    REDICTMODULE_GET_API(ReplyWithEmptyArray);
    REDICTMODULE_GET_API(ReplySetArrayLength);
    REDICTMODULE_GET_API(ReplySetMapLength);
    REDICTMODULE_GET_API(ReplySetSetLength);
    REDICTMODULE_GET_API(ReplySetAttributeLength);
    REDICTMODULE_GET_API(ReplySetPushLength);
    REDICTMODULE_GET_API(ReplyWithStringBuffer);
    REDICTMODULE_GET_API(ReplyWithCString);
    REDICTMODULE_GET_API(ReplyWithString);
    REDICTMODULE_GET_API(ReplyWithEmptyString);
    REDICTMODULE_GET_API(ReplyWithVerbatimString);
    REDICTMODULE_GET_API(ReplyWithVerbatimStringType);
    REDICTMODULE_GET_API(ReplyWithNull);
    REDICTMODULE_GET_API(ReplyWithBool);
    REDICTMODULE_GET_API(ReplyWithCallReply);
    REDICTMODULE_GET_API(ReplyWithDouble);
    REDICTMODULE_GET_API(ReplyWithBigNumber);
    REDICTMODULE_GET_API(ReplyWithLongDouble);
    REDICTMODULE_GET_API(GetSelectedDb);
    REDICTMODULE_GET_API(SelectDb);
    REDICTMODULE_GET_API(KeyExists);
    REDICTMODULE_GET_API(OpenKey);
    REDICTMODULE_GET_API(GetOpenKeyModesAll);
    REDICTMODULE_GET_API(CloseKey);
    REDICTMODULE_GET_API(KeyType);
    REDICTMODULE_GET_API(ValueLength);
    REDICTMODULE_GET_API(ListPush);
    REDICTMODULE_GET_API(ListPop);
    REDICTMODULE_GET_API(ListGet);
    REDICTMODULE_GET_API(ListSet);
    REDICTMODULE_GET_API(ListInsert);
    REDICTMODULE_GET_API(ListDelete);
    REDICTMODULE_GET_API(StringToLongLong);
    REDICTMODULE_GET_API(StringToULongLong);
    REDICTMODULE_GET_API(StringToDouble);
    REDICTMODULE_GET_API(StringToLongDouble);
    REDICTMODULE_GET_API(StringToStreamID);
    REDICTMODULE_GET_API(Call);
    REDICTMODULE_GET_API(CallReplyProto);
    REDICTMODULE_GET_API(FreeCallReply);
    REDICTMODULE_GET_API(CallReplyInteger);
    REDICTMODULE_GET_API(CallReplyDouble);
    REDICTMODULE_GET_API(CallReplyBool);
    REDICTMODULE_GET_API(CallReplyBigNumber);
    REDICTMODULE_GET_API(CallReplyVerbatim);
    REDICTMODULE_GET_API(CallReplySetElement);
    REDICTMODULE_GET_API(CallReplyMapElement);
    REDICTMODULE_GET_API(CallReplyAttributeElement);
    REDICTMODULE_GET_API(CallReplyPromiseSetUnblockHandler);
    REDICTMODULE_GET_API(CallReplyPromiseAbort);
    REDICTMODULE_GET_API(CallReplyAttribute);
    REDICTMODULE_GET_API(CallReplyType);
    REDICTMODULE_GET_API(CallReplyLength);
    REDICTMODULE_GET_API(CallReplyArrayElement);
    REDICTMODULE_GET_API(CallReplyStringPtr);
    REDICTMODULE_GET_API(CreateStringFromCallReply);
    REDICTMODULE_GET_API(CreateString);
    REDICTMODULE_GET_API(CreateStringFromLongLong);
    REDICTMODULE_GET_API(CreateStringFromULongLong);
    REDICTMODULE_GET_API(CreateStringFromDouble);
    REDICTMODULE_GET_API(CreateStringFromLongDouble);
    REDICTMODULE_GET_API(CreateStringFromString);
    REDICTMODULE_GET_API(CreateStringFromStreamID);
    REDICTMODULE_GET_API(CreateStringPrintf);
    REDICTMODULE_GET_API(FreeString);
    REDICTMODULE_GET_API(StringPtrLen);
    REDICTMODULE_GET_API(AutoMemory);
    REDICTMODULE_GET_API(Replicate);
    REDICTMODULE_GET_API(ReplicateVerbatim);
    REDICTMODULE_GET_API(DeleteKey);
    REDICTMODULE_GET_API(UnlinkKey);
    REDICTMODULE_GET_API(StringSet);
    REDICTMODULE_GET_API(StringDMA);
    REDICTMODULE_GET_API(StringTruncate);
    REDICTMODULE_GET_API(GetExpire);
    REDICTMODULE_GET_API(SetExpire);
    REDICTMODULE_GET_API(GetAbsExpire);
    REDICTMODULE_GET_API(SetAbsExpire);
    REDICTMODULE_GET_API(ResetDataset);
    REDICTMODULE_GET_API(DbSize);
    REDICTMODULE_GET_API(RandomKey);
    REDICTMODULE_GET_API(ZsetAdd);
    REDICTMODULE_GET_API(ZsetIncrby);
    REDICTMODULE_GET_API(ZsetScore);
    REDICTMODULE_GET_API(ZsetRem);
    REDICTMODULE_GET_API(ZsetRangeStop);
    REDICTMODULE_GET_API(ZsetFirstInScoreRange);
    REDICTMODULE_GET_API(ZsetLastInScoreRange);
    REDICTMODULE_GET_API(ZsetFirstInLexRange);
    REDICTMODULE_GET_API(ZsetLastInLexRange);
    REDICTMODULE_GET_API(ZsetRangeCurrentElement);
    REDICTMODULE_GET_API(ZsetRangeNext);
    REDICTMODULE_GET_API(ZsetRangePrev);
    REDICTMODULE_GET_API(ZsetRangeEndReached);
    REDICTMODULE_GET_API(HashSet);
    REDICTMODULE_GET_API(HashGet);
    REDICTMODULE_GET_API(StreamAdd);
    REDICTMODULE_GET_API(StreamDelete);
    REDICTMODULE_GET_API(StreamIteratorStart);
    REDICTMODULE_GET_API(StreamIteratorStop);
    REDICTMODULE_GET_API(StreamIteratorNextID);
    REDICTMODULE_GET_API(StreamIteratorNextField);
    REDICTMODULE_GET_API(StreamIteratorDelete);
    REDICTMODULE_GET_API(StreamTrimByLength);
    REDICTMODULE_GET_API(StreamTrimByID);
    REDICTMODULE_GET_API(IsKeysPositionRequest);
    REDICTMODULE_GET_API(KeyAtPos);
    REDICTMODULE_GET_API(KeyAtPosWithFlags);
    REDICTMODULE_GET_API(IsChannelsPositionRequest);
    REDICTMODULE_GET_API(ChannelAtPosWithFlags);
    REDICTMODULE_GET_API(GetClientId);
    REDICTMODULE_GET_API(GetClientUserNameById);
    REDICTMODULE_GET_API(GetContextFlags);
    REDICTMODULE_GET_API(AvoidReplicaTraffic);
    REDICTMODULE_GET_API(PoolAlloc);
    REDICTMODULE_GET_API(CreateDataType);
    REDICTMODULE_GET_API(ModuleTypeSetValue);
    REDICTMODULE_GET_API(ModuleTypeReplaceValue);
    REDICTMODULE_GET_API(ModuleTypeGetType);
    REDICTMODULE_GET_API(ModuleTypeGetValue);
    REDICTMODULE_GET_API(IsIOError);
    REDICTMODULE_GET_API(SetModuleOptions);
    REDICTMODULE_GET_API(SignalModifiedKey);
    REDICTMODULE_GET_API(SaveUnsigned);
    REDICTMODULE_GET_API(LoadUnsigned);
    REDICTMODULE_GET_API(SaveSigned);
    REDICTMODULE_GET_API(LoadSigned);
    REDICTMODULE_GET_API(SaveString);
    REDICTMODULE_GET_API(SaveStringBuffer);
    REDICTMODULE_GET_API(LoadString);
    REDICTMODULE_GET_API(LoadStringBuffer);
    REDICTMODULE_GET_API(SaveDouble);
    REDICTMODULE_GET_API(LoadDouble);
    REDICTMODULE_GET_API(SaveFloat);
    REDICTMODULE_GET_API(LoadFloat);
    REDICTMODULE_GET_API(SaveLongDouble);
    REDICTMODULE_GET_API(LoadLongDouble);
    REDICTMODULE_GET_API(SaveDataTypeToString);
    REDICTMODULE_GET_API(LoadDataTypeFromString);
    REDICTMODULE_GET_API(LoadDataTypeFromStringEncver);
    REDICTMODULE_GET_API(EmitAOF);
    REDICTMODULE_GET_API(Log);
    REDICTMODULE_GET_API(LogIOError);
    REDICTMODULE_GET_API(_Assert);
    REDICTMODULE_GET_API(LatencyAddSample);
    REDICTMODULE_GET_API(StringAppendBuffer);
    REDICTMODULE_GET_API(TrimStringAllocation);
    REDICTMODULE_GET_API(RetainString);
    REDICTMODULE_GET_API(HoldString);
    REDICTMODULE_GET_API(StringCompare);
    REDICTMODULE_GET_API(GetContextFromIO);
    REDICTMODULE_GET_API(GetKeyNameFromIO);
    REDICTMODULE_GET_API(GetKeyNameFromModuleKey);
    REDICTMODULE_GET_API(GetDbIdFromModuleKey);
    REDICTMODULE_GET_API(GetDbIdFromIO);
    REDICTMODULE_GET_API(GetKeyNameFromOptCtx);
    REDICTMODULE_GET_API(GetToKeyNameFromOptCtx);
    REDICTMODULE_GET_API(GetDbIdFromOptCtx);
    REDICTMODULE_GET_API(GetToDbIdFromOptCtx);
    REDICTMODULE_GET_API(Milliseconds);
    REDICTMODULE_GET_API(MonotonicMicroseconds);
    REDICTMODULE_GET_API(Microseconds);
    REDICTMODULE_GET_API(CachedMicroseconds);
    REDICTMODULE_GET_API(DigestAddStringBuffer);
    REDICTMODULE_GET_API(DigestAddLongLong);
    REDICTMODULE_GET_API(DigestEndSequence);
    REDICTMODULE_GET_API(GetKeyNameFromDigest);
    REDICTMODULE_GET_API(GetDbIdFromDigest);
    REDICTMODULE_GET_API(CreateDict);
    REDICTMODULE_GET_API(FreeDict);
    REDICTMODULE_GET_API(DictSize);
    REDICTMODULE_GET_API(DictSetC);
    REDICTMODULE_GET_API(DictReplaceC);
    REDICTMODULE_GET_API(DictSet);
    REDICTMODULE_GET_API(DictReplace);
    REDICTMODULE_GET_API(DictGetC);
    REDICTMODULE_GET_API(DictGet);
    REDICTMODULE_GET_API(DictDelC);
    REDICTMODULE_GET_API(DictDel);
    REDICTMODULE_GET_API(DictIteratorStartC);
    REDICTMODULE_GET_API(DictIteratorStart);
    REDICTMODULE_GET_API(DictIteratorStop);
    REDICTMODULE_GET_API(DictIteratorReseekC);
    REDICTMODULE_GET_API(DictIteratorReseek);
    REDICTMODULE_GET_API(DictNextC);
    REDICTMODULE_GET_API(DictPrevC);
    REDICTMODULE_GET_API(DictNext);
    REDICTMODULE_GET_API(DictPrev);
    REDICTMODULE_GET_API(DictCompare);
    REDICTMODULE_GET_API(DictCompareC);
    REDICTMODULE_GET_API(RegisterInfoFunc);
    REDICTMODULE_GET_API(RegisterAuthCallback);
    REDICTMODULE_GET_API(InfoAddSection);
    REDICTMODULE_GET_API(InfoBeginDictField);
    REDICTMODULE_GET_API(InfoEndDictField);
    REDICTMODULE_GET_API(InfoAddFieldString);
    REDICTMODULE_GET_API(InfoAddFieldCString);
    REDICTMODULE_GET_API(InfoAddFieldDouble);
    REDICTMODULE_GET_API(InfoAddFieldLongLong);
    REDICTMODULE_GET_API(InfoAddFieldULongLong);
    REDICTMODULE_GET_API(GetServerInfo);
    REDICTMODULE_GET_API(FreeServerInfo);
    REDICTMODULE_GET_API(ServerInfoGetField);
    REDICTMODULE_GET_API(ServerInfoGetFieldC);
    REDICTMODULE_GET_API(ServerInfoGetFieldSigned);
    REDICTMODULE_GET_API(ServerInfoGetFieldUnsigned);
    REDICTMODULE_GET_API(ServerInfoGetFieldDouble);
    REDICTMODULE_GET_API(GetClientInfoById);
    REDICTMODULE_GET_API(GetClientNameById);
    REDICTMODULE_GET_API(SetClientNameById);
    REDICTMODULE_GET_API(PublishMessage);
    REDICTMODULE_GET_API(PublishMessageShard);
    REDICTMODULE_GET_API(SubscribeToServerEvent);
    REDICTMODULE_GET_API(SetLRU);
    REDICTMODULE_GET_API(GetLRU);
    REDICTMODULE_GET_API(SetLFU);
    REDICTMODULE_GET_API(GetLFU);
    REDICTMODULE_GET_API(BlockClientOnKeys);
    REDICTMODULE_GET_API(BlockClientOnKeysWithFlags);
    REDICTMODULE_GET_API(SignalKeyAsReady);
    REDICTMODULE_GET_API(GetBlockedClientReadyKey);
    REDICTMODULE_GET_API(ScanCursorCreate);
    REDICTMODULE_GET_API(ScanCursorRestart);
    REDICTMODULE_GET_API(ScanCursorDestroy);
    REDICTMODULE_GET_API(Scan);
    REDICTMODULE_GET_API(ScanKey);
    REDICTMODULE_GET_API(GetContextFlagsAll);
    REDICTMODULE_GET_API(GetModuleOptionsAll);
    REDICTMODULE_GET_API(GetKeyspaceNotificationFlagsAll);
    REDICTMODULE_GET_API(IsSubEventSupported);
    REDICTMODULE_GET_API(GetServerVersion);
    REDICTMODULE_GET_API(GetTypeMethodVersion);
    REDICTMODULE_GET_API(Yield);
    REDICTMODULE_GET_API(GetThreadSafeContext);
    REDICTMODULE_GET_API(GetDetachedThreadSafeContext);
    REDICTMODULE_GET_API(FreeThreadSafeContext);
    REDICTMODULE_GET_API(ThreadSafeContextLock);
    REDICTMODULE_GET_API(ThreadSafeContextTryLock);
    REDICTMODULE_GET_API(ThreadSafeContextUnlock);
    REDICTMODULE_GET_API(BlockClient);
    REDICTMODULE_GET_API(BlockClientGetPrivateData);
    REDICTMODULE_GET_API(BlockClientSetPrivateData);
    REDICTMODULE_GET_API(BlockClientOnAuth);
    REDICTMODULE_GET_API(UnblockClient);
    REDICTMODULE_GET_API(IsBlockedReplyRequest);
    REDICTMODULE_GET_API(IsBlockedTimeoutRequest);
    REDICTMODULE_GET_API(GetBlockedClientPrivateData);
    REDICTMODULE_GET_API(GetBlockedClientHandle);
    REDICTMODULE_GET_API(AbortBlock);
    REDICTMODULE_GET_API(BlockedClientMeasureTimeStart);
    REDICTMODULE_GET_API(BlockedClientMeasureTimeEnd);
    REDICTMODULE_GET_API(SetDisconnectCallback);
    REDICTMODULE_GET_API(SubscribeToKeyspaceEvents);
    REDICTMODULE_GET_API(AddPostNotificationJob);
    REDICTMODULE_GET_API(NotifyKeyspaceEvent);
    REDICTMODULE_GET_API(GetNotifyKeyspaceEvents);
    REDICTMODULE_GET_API(BlockedClientDisconnected);
    REDICTMODULE_GET_API(RegisterClusterMessageReceiver);
    REDICTMODULE_GET_API(SendClusterMessage);
    REDICTMODULE_GET_API(GetClusterNodeInfo);
    REDICTMODULE_GET_API(GetClusterNodesList);
    REDICTMODULE_GET_API(FreeClusterNodesList);
    REDICTMODULE_GET_API(CreateTimer);
    REDICTMODULE_GET_API(StopTimer);
    REDICTMODULE_GET_API(GetTimerInfo);
    REDICTMODULE_GET_API(GetMyClusterID);
    REDICTMODULE_GET_API(GetClusterSize);
    REDICTMODULE_GET_API(GetRandomBytes);
    REDICTMODULE_GET_API(GetRandomHexChars);
    REDICTMODULE_GET_API(SetClusterFlags);
    REDICTMODULE_GET_API(ClusterKeySlot);
    REDICTMODULE_GET_API(ClusterCanonicalKeyNameInSlot);
    REDICTMODULE_GET_API(ExportSharedAPI);
    REDICTMODULE_GET_API(GetSharedAPI);
    REDICTMODULE_GET_API(RegisterCommandFilter);
    REDICTMODULE_GET_API(UnregisterCommandFilter);
    REDICTMODULE_GET_API(CommandFilterArgsCount);
    REDICTMODULE_GET_API(CommandFilterArgGet);
    REDICTMODULE_GET_API(CommandFilterArgInsert);
    REDICTMODULE_GET_API(CommandFilterArgReplace);
    REDICTMODULE_GET_API(CommandFilterArgDelete);
    REDICTMODULE_GET_API(CommandFilterGetClientId);
    REDICTMODULE_GET_API(Fork);
    REDICTMODULE_GET_API(SendChildHeartbeat);
    REDICTMODULE_GET_API(ExitFromChild);
    REDICTMODULE_GET_API(KillForkChild);
    REDICTMODULE_GET_API(GetUsedMemoryRatio);
    REDICTMODULE_GET_API(MallocSize);
    REDICTMODULE_GET_API(MallocUsableSize);
    REDICTMODULE_GET_API(MallocSizeString);
    REDICTMODULE_GET_API(MallocSizeDict);
    REDICTMODULE_GET_API(CreateModuleUser);
    REDICTMODULE_GET_API(FreeModuleUser);
    REDICTMODULE_GET_API(SetContextUser);
    REDICTMODULE_GET_API(SetModuleUserACL);
    REDICTMODULE_GET_API(SetModuleUserACLString);
    REDICTMODULE_GET_API(GetModuleUserACLString);
    REDICTMODULE_GET_API(GetCurrentUserName);
    REDICTMODULE_GET_API(GetModuleUserFromUserName);
    REDICTMODULE_GET_API(ACLCheckCommandPermissions);
    REDICTMODULE_GET_API(ACLCheckKeyPermissions);
    REDICTMODULE_GET_API(ACLCheckChannelPermissions);
    REDICTMODULE_GET_API(ACLAddLogEntry);
    REDICTMODULE_GET_API(ACLAddLogEntryByUserName);
    REDICTMODULE_GET_API(DeauthenticateAndCloseClient);
    REDICTMODULE_GET_API(AuthenticateClientWithACLUser);
    REDICTMODULE_GET_API(AuthenticateClientWithUser);
    REDICTMODULE_GET_API(RedactClientCommandArgument);
    REDICTMODULE_GET_API(GetClientCertificate);
    REDICTMODULE_GET_API(GetCommandKeys);
    REDICTMODULE_GET_API(GetCommandKeysWithFlags);
    REDICTMODULE_GET_API(GetCurrentCommandName);
    REDICTMODULE_GET_API(RegisterDefragFunc);
    REDICTMODULE_GET_API(DefragAlloc);
    REDICTMODULE_GET_API(DefragRedictModuleString);
    REDICTMODULE_GET_API(DefragShouldStop);
    REDICTMODULE_GET_API(DefragCursorSet);
    REDICTMODULE_GET_API(DefragCursorGet);
    REDICTMODULE_GET_API(GetKeyNameFromDefragCtx);
    REDICTMODULE_GET_API(GetDbIdFromDefragCtx);
    REDICTMODULE_GET_API(EventLoopAdd);
    REDICTMODULE_GET_API(EventLoopDel);
    REDICTMODULE_GET_API(EventLoopAddOneShot);
    REDICTMODULE_GET_API(RegisterBoolConfig);
    REDICTMODULE_GET_API(RegisterNumericConfig);
    REDICTMODULE_GET_API(RegisterStringConfig);
    REDICTMODULE_GET_API(RegisterEnumConfig);
    REDICTMODULE_GET_API(LoadConfigs);
    REDICTMODULE_GET_API(RdbStreamCreateFromFile);
    REDICTMODULE_GET_API(RdbStreamFree);
    REDICTMODULE_GET_API(RdbLoad);
    REDICTMODULE_GET_API(RdbSave);

    if (RedictModule_IsModuleNameBusy && RedictModule_IsModuleNameBusy(name)) return REDICTMODULE_ERR;
    RedictModule_SetModuleAttribs(ctx,name,ver,apiver);
    return REDICTMODULE_OK;
}

#define RedictModule_Assert(_e) ((_e)?(void)0 : (RedictModule__Assert(#_e,__FILE__,__LINE__),exit(1)))

#define RMAPI_FUNC_SUPPORTED(func) (func != NULL)

#endif /* REDICTMODULE_CORE */
#endif /* REDICTMODULE_H */
