# SPDX-FileCopyrightText: 2024 Redict Contributors
# SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
#
# SPDX-License-Identifier: BSD-3-Clause
# SPDX-License-Identifier: LGPL-3.0-only

start_server {} {
    set i [r info]
    regexp {redict_version:(.*?)\r\n} $i - version
    regexp {redict_git_sha1:(.*?)\r\n} $i - sha1
    puts "Testing Redict version $version ($sha1)"
}
