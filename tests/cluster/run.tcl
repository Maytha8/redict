# Copyright (C) 2014 Salvatore Sanfilippo antirez@gmail.com
# SPDX-FileCopyrightText: 2024 Redict Contributors
# SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
#
# SPDX-License-Identifier: BSD-3-Clause
# SPDX-License-Identifier: LGPL-3.0-only

cd tests/cluster
source cluster.tcl
source ../instances.tcl
source ../../support/cluster.tcl ; # Redict Cluster client.

set ::instances_count 20 ; # How many instances we use at max.
set ::tlsdir "../../tls"

proc main {} {
    parse_options
    spawn_instance redict $::redict_base_port $::instances_count {
        "cluster-enabled yes"
        "appendonly yes"
        "enable-protected-configs yes"
        "enable-debug-command yes"
        "save ''"
    }
    run_tests
    cleanup
    end_tests
}

if {[catch main e]} {
    puts $::errorInfo
    if {$::pause_on_error} pause_on_error
    cleanup
    exit 1
}
