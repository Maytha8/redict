// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-only

#include "redictmodule.h"

#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>

#define UNUSED(x) (void)(x)

static int n_events = 0;

static int KeySpace_NotificationModuleKeyMissExpired(RedictModuleCtx *ctx, int type, const char *event, RedictModuleString *key) {
    UNUSED(ctx);
    UNUSED(type);
    UNUSED(event);
    UNUSED(key);
    n_events++;
    return REDICTMODULE_OK;
}

int test_clear_n_events(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    UNUSED(argv);
    UNUSED(argc);
    n_events = 0;
    RedictModule_ReplyWithSimpleString(ctx, "OK");
    return REDICTMODULE_OK;
}

int test_get_n_events(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    UNUSED(argv);
    UNUSED(argc);
    RedictModule_ReplyWithLongLong(ctx, n_events);
    return REDICTMODULE_OK;
}

int test_open_key_no_effects(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc<2) {
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }

    int supportedMode = RedictModule_GetOpenKeyModesAll();
    if (!(supportedMode & REDICTMODULE_READ) || !(supportedMode & REDICTMODULE_OPEN_KEY_NOEFFECTS)) {
        RedictModule_ReplyWithError(ctx, "OpenKey modes are not supported");
        return REDICTMODULE_OK;
    }

    RedictModuleKey *key = RedictModule_OpenKey(ctx, argv[1], REDICTMODULE_READ | REDICTMODULE_OPEN_KEY_NOEFFECTS);
    if (!key) {
        RedictModule_ReplyWithError(ctx, "key not found");
        return REDICTMODULE_OK;
    }

    RedictModule_CloseKey(key);
    RedictModule_ReplyWithSimpleString(ctx, "OK");
    return REDICTMODULE_OK;
}

int test_call_generic(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    if (argc<2) {
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }

    const char* cmdname = RedictModule_StringPtrLen(argv[1], NULL);
    RedictModuleCallReply *reply = RedictModule_Call(ctx, cmdname, "v", argv+2, argc-2);
    if (reply) {
        RedictModule_ReplyWithCallReply(ctx, reply);
        RedictModule_FreeCallReply(reply);
    } else {
        RedictModule_ReplyWithError(ctx, strerror(errno));
    }
    return REDICTMODULE_OK;
}

int test_call_info(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    RedictModuleCallReply *reply;
    if (argc>1)
        reply = RedictModule_Call(ctx, "info", "s", argv[1]);
    else
        reply = RedictModule_Call(ctx, "info", "");
    if (reply) {
        RedictModule_ReplyWithCallReply(ctx, reply);
        RedictModule_FreeCallReply(reply);
    } else {
        RedictModule_ReplyWithError(ctx, strerror(errno));
    }
    return REDICTMODULE_OK;
}

int test_ld_conv(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    UNUSED(argv);
    UNUSED(argc);
    long double ld = 0.00000000000000001L;
    const char *ldstr = "0.00000000000000001";
    RedictModuleString *s1 = RedictModule_CreateStringFromLongDouble(ctx, ld, 1);
    RedictModuleString *s2 =
        RedictModule_CreateString(ctx, ldstr, strlen(ldstr));
    if (RedictModule_StringCompare(s1, s2) != 0) {
        char err[4096];
        snprintf(err, 4096,
            "Failed to convert long double to string ('%s' != '%s')",
            RedictModule_StringPtrLen(s1, NULL),
            RedictModule_StringPtrLen(s2, NULL));
        RedictModule_ReplyWithError(ctx, err);
        goto final;
    }
    long double ld2 = 0;
    if (RedictModule_StringToLongDouble(s2, &ld2) == REDICTMODULE_ERR) {
        RedictModule_ReplyWithError(ctx,
            "Failed to convert string to long double");
        goto final;
    }
    if (ld2 != ld) {
        char err[4096];
        snprintf(err, 4096,
            "Failed to convert string to long double (%.40Lf != %.40Lf)",
            ld2,
            ld);
        RedictModule_ReplyWithError(ctx, err);
        goto final;
    }

    /* Make sure we can't convert a string that has \0 in it */
    char buf[4] = "123";
    buf[1] = '\0';
    RedictModuleString *s3 = RedictModule_CreateString(ctx, buf, 3);
    long double ld3;
    if (RedictModule_StringToLongDouble(s3, &ld3) == REDICTMODULE_OK) {
        RedictModule_ReplyWithError(ctx, "Invalid string successfully converted to long double");
        RedictModule_FreeString(ctx, s3);
        goto final;
    }
    RedictModule_FreeString(ctx, s3);

    RedictModule_ReplyWithLongDouble(ctx, ld2);
final:
    RedictModule_FreeString(ctx, s1);
    RedictModule_FreeString(ctx, s2);
    return REDICTMODULE_OK;
}

int test_flushall(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);
    RedictModule_ResetDataset(1, 0);
    RedictModule_ReplyWithCString(ctx, "Ok");
    return REDICTMODULE_OK;
}

int test_dbsize(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);
    long long ll = RedictModule_DbSize(ctx);
    RedictModule_ReplyWithLongLong(ctx, ll);
    return REDICTMODULE_OK;
}

int test_randomkey(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);
    RedictModuleString *str = RedictModule_RandomKey(ctx);
    RedictModule_ReplyWithString(ctx, str);
    RedictModule_FreeString(ctx, str);
    return REDICTMODULE_OK;
}

int test_keyexists(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc < 2) return RedictModule_WrongArity(ctx);
    RedictModuleString *key = argv[1];
    int exists = RedictModule_KeyExists(ctx, key);
    return RedictModule_ReplyWithBool(ctx, exists);
}

RedictModuleKey *open_key_or_reply(RedictModuleCtx *ctx, RedictModuleString *keyname, int mode) {
    RedictModuleKey *key = RedictModule_OpenKey(ctx, keyname, mode);
    if (!key) {
        RedictModule_ReplyWithError(ctx, "key not found");
        return NULL;
    }
    return key;
}

int test_getlru(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    if (argc<2) {
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }
    RedictModuleKey *key = open_key_or_reply(ctx, argv[1], REDICTMODULE_READ|REDICTMODULE_OPEN_KEY_NOTOUCH);
    mstime_t lru;
    RedictModule_GetLRU(key, &lru);
    RedictModule_ReplyWithLongLong(ctx, lru);
    RedictModule_CloseKey(key);
    return REDICTMODULE_OK;
}

int test_setlru(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    if (argc<3) {
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }
    RedictModuleKey *key = open_key_or_reply(ctx, argv[1], REDICTMODULE_READ|REDICTMODULE_OPEN_KEY_NOTOUCH);
    mstime_t lru;
    if (RedictModule_StringToLongLong(argv[2], &lru) != REDICTMODULE_OK) {
        RedictModule_ReplyWithError(ctx, "invalid idle time");
        return REDICTMODULE_OK;
    }
    int was_set = RedictModule_SetLRU(key, lru)==REDICTMODULE_OK;
    RedictModule_ReplyWithLongLong(ctx, was_set);
    RedictModule_CloseKey(key);
    return REDICTMODULE_OK;
}

int test_getlfu(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    if (argc<2) {
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }
    RedictModuleKey *key = open_key_or_reply(ctx, argv[1], REDICTMODULE_READ|REDICTMODULE_OPEN_KEY_NOTOUCH);
    mstime_t lfu;
    RedictModule_GetLFU(key, &lfu);
    RedictModule_ReplyWithLongLong(ctx, lfu);
    RedictModule_CloseKey(key);
    return REDICTMODULE_OK;
}

int test_setlfu(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    if (argc<3) {
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }
    RedictModuleKey *key = open_key_or_reply(ctx, argv[1], REDICTMODULE_READ|REDICTMODULE_OPEN_KEY_NOTOUCH);
    mstime_t lfu;
    if (RedictModule_StringToLongLong(argv[2], &lfu) != REDICTMODULE_OK) {
        RedictModule_ReplyWithError(ctx, "invalid freq");
        return REDICTMODULE_OK;
    }
    int was_set = RedictModule_SetLFU(key, lfu)==REDICTMODULE_OK;
    RedictModule_ReplyWithLongLong(ctx, was_set);
    RedictModule_CloseKey(key);
    return REDICTMODULE_OK;
}

int test_redictversion(RedictModuleCtx *ctx, RedictModuleString **argv, int argc){
    (void) argv;
    (void) argc;

    int version = RedictModule_GetServerVersion();
    int patch = version & 0x000000ff;
    int minor = (version & 0x0000ff00) >> 8;
    int major = (version & 0x00ff0000) >> 16;

    RedictModuleString* vStr = RedictModule_CreateStringPrintf(ctx, "%d.%d.%d", major, minor, patch);
    RedictModule_ReplyWithString(ctx, vStr);
    RedictModule_FreeString(ctx, vStr);

    return REDICTMODULE_OK;
}

int test_getclientcert(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    (void) argv;
    (void) argc;

    RedictModuleString *cert = RedictModule_GetClientCertificate(ctx,
            RedictModule_GetClientId(ctx));
    if (!cert) {
        RedictModule_ReplyWithNull(ctx);
    } else {
        RedictModule_ReplyWithString(ctx, cert);
        RedictModule_FreeString(ctx, cert);
    }

    return REDICTMODULE_OK;
}

int test_clientinfo(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    (void) argv;
    (void) argc;

    RedictModuleClientInfoV1 ci = REDICTMODULE_CLIENTINFO_INITIALIZER_V1;
    uint64_t client_id = RedictModule_GetClientId(ctx);

    /* Check expected result from the V1 initializer. */
    assert(ci.version == 1);
    /* Trying to populate a future version of the struct should fail. */
    ci.version = REDICTMODULE_CLIENTINFO_VERSION + 1;
    assert(RedictModule_GetClientInfoById(&ci, client_id) == REDICTMODULE_ERR);

    ci.version = 1;
    if (RedictModule_GetClientInfoById(&ci, client_id) == REDICTMODULE_ERR) {
            RedictModule_ReplyWithError(ctx, "failed to get client info");
            return REDICTMODULE_OK;
    }

    RedictModule_ReplyWithArray(ctx, 10);
    char flags[512];
    snprintf(flags, sizeof(flags) - 1, "%s:%s:%s:%s:%s:%s",
        ci.flags & REDICTMODULE_CLIENTINFO_FLAG_SSL ? "ssl" : "",
        ci.flags & REDICTMODULE_CLIENTINFO_FLAG_PUBSUB ? "pubsub" : "",
        ci.flags & REDICTMODULE_CLIENTINFO_FLAG_BLOCKED ? "blocked" : "",
        ci.flags & REDICTMODULE_CLIENTINFO_FLAG_TRACKING ? "tracking" : "",
        ci.flags & REDICTMODULE_CLIENTINFO_FLAG_UNIXSOCKET ? "unixsocket" : "",
        ci.flags & REDICTMODULE_CLIENTINFO_FLAG_MULTI ? "multi" : "");

    RedictModule_ReplyWithCString(ctx, "flags");
    RedictModule_ReplyWithCString(ctx, flags);
    RedictModule_ReplyWithCString(ctx, "id");
    RedictModule_ReplyWithLongLong(ctx, ci.id);
    RedictModule_ReplyWithCString(ctx, "addr");
    RedictModule_ReplyWithCString(ctx, ci.addr);
    RedictModule_ReplyWithCString(ctx, "port");
    RedictModule_ReplyWithLongLong(ctx, ci.port);
    RedictModule_ReplyWithCString(ctx, "db");
    RedictModule_ReplyWithLongLong(ctx, ci.db);

    return REDICTMODULE_OK;
}

int test_getname(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    (void)argv;
    if (argc != 1) return RedictModule_WrongArity(ctx);
    unsigned long long id = RedictModule_GetClientId(ctx);
    RedictModuleString *name = RedictModule_GetClientNameById(ctx, id);
    if (name == NULL)
        return RedictModule_ReplyWithError(ctx, "-ERR No name");
    RedictModule_ReplyWithString(ctx, name);
    RedictModule_FreeString(ctx, name);
    return REDICTMODULE_OK;
}

int test_setname(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc != 2) return RedictModule_WrongArity(ctx);
    unsigned long long id = RedictModule_GetClientId(ctx);
    if (RedictModule_SetClientNameById(id, argv[1]) == REDICTMODULE_OK)
        return RedictModule_ReplyWithSimpleString(ctx, "OK");
    else
        return RedictModule_ReplyWithError(ctx, strerror(errno));
}

int test_log_tsctx(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    RedictModuleCtx *tsctx = RedictModule_GetDetachedThreadSafeContext(ctx);

    if (argc != 3) {
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }

    char level[50];
    size_t level_len;
    const char *level_str = RedictModule_StringPtrLen(argv[1], &level_len);
    snprintf(level, sizeof(level) - 1, "%.*s", (int) level_len, level_str);

    size_t msg_len;
    const char *msg_str = RedictModule_StringPtrLen(argv[2], &msg_len);

    RedictModule_Log(tsctx, level, "%.*s", (int) msg_len, msg_str);
    RedictModule_FreeThreadSafeContext(tsctx);

    RedictModule_ReplyWithSimpleString(ctx, "OK");
    return REDICTMODULE_OK;
}

int test_weird_cmd(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    RedictModule_ReplyWithSimpleString(ctx, "OK");
    return REDICTMODULE_OK;
}

int test_monotonic_time(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    RedictModule_ReplyWithLongLong(ctx, RedictModule_MonotonicMicroseconds());
    return REDICTMODULE_OK;
}

/* wrapper for RM_Call */
int test_rm_call(RedictModuleCtx *ctx, RedictModuleString **argv, int argc){
    if(argc < 2){
        return RedictModule_WrongArity(ctx);
    }

    const char* cmd = RedictModule_StringPtrLen(argv[1], NULL);

    RedictModuleCallReply* rep = RedictModule_Call(ctx, cmd, "Ev", argv + 2, argc - 2);
    if(!rep){
        RedictModule_ReplyWithError(ctx, "NULL reply returned");
    }else{
        RedictModule_ReplyWithCallReply(ctx, rep);
        RedictModule_FreeCallReply(rep);
    }

    return REDICTMODULE_OK;
}

/* wrapper for RM_Call which also replicates the module command */
int test_rm_call_replicate(RedictModuleCtx *ctx, RedictModuleString **argv, int argc){
    test_rm_call(ctx, argv, argc);
    RedictModule_ReplicateVerbatim(ctx);

    return REDICTMODULE_OK;
}

/* wrapper for RM_Call with flags */
int test_rm_call_flags(RedictModuleCtx *ctx, RedictModuleString **argv, int argc){
    if(argc < 3){
        return RedictModule_WrongArity(ctx);
    }

    /* Append Ev to the provided flags. */
    RedictModuleString *flags = RedictModule_CreateStringFromString(ctx, argv[1]);
    RedictModule_StringAppendBuffer(ctx, flags, "Ev", 2);

    const char* flg = RedictModule_StringPtrLen(flags, NULL);
    const char* cmd = RedictModule_StringPtrLen(argv[2], NULL);

    RedictModuleCallReply* rep = RedictModule_Call(ctx, cmd, flg, argv + 3, argc - 3);
    if(!rep){
        RedictModule_ReplyWithError(ctx, "NULL reply returned");
    }else{
        RedictModule_ReplyWithCallReply(ctx, rep);
        RedictModule_FreeCallReply(rep);
    }
    RedictModule_FreeString(ctx, flags);

    return REDICTMODULE_OK;
}

int test_ull_conv(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    UNUSED(argv);
    UNUSED(argc);
    unsigned long long ull = 18446744073709551615ULL;
    const char *ullstr = "18446744073709551615";

    RedictModuleString *s1 = RedictModule_CreateStringFromULongLong(ctx, ull);
    RedictModuleString *s2 =
        RedictModule_CreateString(ctx, ullstr, strlen(ullstr));
    if (RedictModule_StringCompare(s1, s2) != 0) {
        char err[4096];
        snprintf(err, 4096,
            "Failed to convert unsigned long long to string ('%s' != '%s')",
            RedictModule_StringPtrLen(s1, NULL),
            RedictModule_StringPtrLen(s2, NULL));
        RedictModule_ReplyWithError(ctx, err);
        goto final;
    }
    unsigned long long ull2 = 0;
    if (RedictModule_StringToULongLong(s2, &ull2) == REDICTMODULE_ERR) {
        RedictModule_ReplyWithError(ctx,
            "Failed to convert string to unsigned long long");
        goto final;
    }
    if (ull2 != ull) {
        char err[4096];
        snprintf(err, 4096,
            "Failed to convert string to unsigned long long (%llu != %llu)",
            ull2,
            ull);
        RedictModule_ReplyWithError(ctx, err);
        goto final;
    }

    /* Make sure we can't convert a string more than ULLONG_MAX or less than 0 */
    ullstr = "18446744073709551616";
    RedictModuleString *s3 = RedictModule_CreateString(ctx, ullstr, strlen(ullstr));
    unsigned long long ull3;
    if (RedictModule_StringToULongLong(s3, &ull3) == REDICTMODULE_OK) {
        RedictModule_ReplyWithError(ctx, "Invalid string successfully converted to unsigned long long");
        RedictModule_FreeString(ctx, s3);
        goto final;
    }
    RedictModule_FreeString(ctx, s3);
    ullstr = "-1";
    RedictModuleString *s4 = RedictModule_CreateString(ctx, ullstr, strlen(ullstr));
    unsigned long long ull4;
    if (RedictModule_StringToULongLong(s4, &ull4) == REDICTMODULE_OK) {
        RedictModule_ReplyWithError(ctx, "Invalid string successfully converted to unsigned long long");
        RedictModule_FreeString(ctx, s4);
        goto final;
    }
    RedictModule_FreeString(ctx, s4);

    RedictModule_ReplyWithSimpleString(ctx, "ok");

final:
    RedictModule_FreeString(ctx, s1);
    RedictModule_FreeString(ctx, s2);
    return REDICTMODULE_OK;
}

int test_malloc_api(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    UNUSED(argv);
    UNUSED(argc);

    void *p;

    p = RedictModule_TryAlloc(1024);
    memset(p, 0, 1024);
    RedictModule_Free(p);

    p = RedictModule_TryCalloc(1, 1024);
    memset(p, 1, 1024);

    p = RedictModule_TryRealloc(p, 5 * 1024);
    memset(p, 1, 5 * 1024);
    RedictModule_Free(p);

    RedictModule_ReplyWithSimpleString(ctx, "OK");
    return REDICTMODULE_OK;
}

int test_keyslot(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    /* Static check of the ClusterKeySlot + ClusterCanonicalKeyNameInSlot
     * round-trip for all slots. */
    for (unsigned int slot = 0; slot < 16384; slot++) {
        const char *tag = RedictModule_ClusterCanonicalKeyNameInSlot(slot);
        RedictModuleString *key = RedictModule_CreateStringPrintf(ctx, "x{%s}y", tag);
        assert(slot == RedictModule_ClusterKeySlot(key));
        RedictModule_FreeString(ctx, key);
    }
    if (argc != 2){
        return RedictModule_WrongArity(ctx);
    }
    unsigned int slot = RedictModule_ClusterKeySlot(argv[1]);
    return RedictModule_ReplyWithLongLong(ctx, slot);
}

int RedictModule_OnLoad(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);
    if (RedictModule_Init(ctx,"misc",1,REDICTMODULE_APIVER_1)== REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if(RedictModule_SubscribeToKeyspaceEvents(ctx, REDICTMODULE_NOTIFY_KEY_MISS | REDICTMODULE_NOTIFY_EXPIRED, KeySpace_NotificationModuleKeyMissExpired) != REDICTMODULE_OK){
        return REDICTMODULE_ERR;
    }

    if (RedictModule_CreateCommand(ctx,"test.call_generic", test_call_generic,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.call_info", test_call_info,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.ld_conversion", test_ld_conv, "",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.ull_conversion", test_ull_conv, "",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.flushall", test_flushall,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.dbsize", test_dbsize,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.randomkey", test_randomkey,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.keyexists", test_keyexists,"",1,1,1) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.setlru", test_setlru,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.getlru", test_getlru,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.setlfu", test_setlfu,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.getlfu", test_getlfu,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.clientinfo", test_clientinfo,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.getname", test_getname,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.setname", test_setname,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.redictversion", test_redictversion,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.getclientcert", test_getclientcert,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.log_tsctx", test_log_tsctx,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    /* Add a command with ':' in it's name, so that we can check commandstats sanitization. */
    if (RedictModule_CreateCommand(ctx,"test.weird:cmd", test_weird_cmd,"readonly",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.monotonic_time", test_monotonic_time,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx, "test.rm_call", test_rm_call,"allow-stale", 0, 0, 0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx, "test.rm_call_flags", test_rm_call_flags,"allow-stale", 0, 0, 0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx, "test.rm_call_replicate", test_rm_call_replicate,"allow-stale", 0, 0, 0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx, "test.silent_open_key", test_open_key_no_effects,"", 0, 0, 0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx, "test.get_n_events", test_get_n_events,"", 0, 0, 0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx, "test.clear_n_events", test_clear_n_events,"", 0, 0, 0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx, "test.malloc_api", test_malloc_api,"", 0, 0, 0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx, "test.keyslot", test_keyslot, "", 0, 0, 0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    return REDICTMODULE_OK;
}
