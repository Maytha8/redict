// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-only

#include "redictmodule.h"

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <memory.h>
#include <errno.h>

/* Sanity tests to verify inputs and return values. */
int sanity(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    RedictModuleRdbStream *s = RedictModule_RdbStreamCreateFromFile("dbnew.rdb");

    /* NULL stream should fail. */
    if (RedictModule_RdbLoad(ctx, NULL, 0) == REDICTMODULE_OK || errno != EINVAL) {
        RedictModule_ReplyWithError(ctx, strerror(errno));
        goto out;
    }

    /* Invalid flags should fail. */
    if (RedictModule_RdbLoad(ctx, s, 188) == REDICTMODULE_OK || errno != EINVAL) {
        RedictModule_ReplyWithError(ctx, strerror(errno));
        goto out;
    }

    /* Missing file should fail. */
    if (RedictModule_RdbLoad(ctx, s, 0) == REDICTMODULE_OK || errno != ENOENT) {
        RedictModule_ReplyWithError(ctx, strerror(errno));
        goto out;
    }

    /* Save RDB file. */
    if (RedictModule_RdbSave(ctx, s, 0) != REDICTMODULE_OK || errno != 0) {
        RedictModule_ReplyWithError(ctx, strerror(errno));
        goto out;
    }

    /* Load the saved RDB file. */
    if (RedictModule_RdbLoad(ctx, s, 0) != REDICTMODULE_OK || errno != 0) {
        RedictModule_ReplyWithError(ctx, strerror(errno));
        goto out;
    }

    RedictModule_ReplyWithSimpleString(ctx, "OK");

 out:
    RedictModule_RdbStreamFree(s);
    return REDICTMODULE_OK;
}

int cmd_rdbsave(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc != 2) {
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }

    size_t len;
    const char *filename = RedictModule_StringPtrLen(argv[1], &len);

    char tmp[len + 1];
    memcpy(tmp, filename, len);
    tmp[len] = '\0';

    RedictModuleRdbStream *stream = RedictModule_RdbStreamCreateFromFile(tmp);

    if (RedictModule_RdbSave(ctx, stream, 0) != REDICTMODULE_OK || errno != 0) {
        RedictModule_ReplyWithError(ctx, strerror(errno));
        goto out;
    }

    RedictModule_ReplyWithSimpleString(ctx, "OK");

out:
    RedictModule_RdbStreamFree(stream);
    return REDICTMODULE_OK;
}

/* Fork before calling RM_RdbSave(). */
int cmd_rdbsave_fork(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc != 2) {
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }

    size_t len;
    const char *filename = RedictModule_StringPtrLen(argv[1], &len);

    char tmp[len + 1];
    memcpy(tmp, filename, len);
    tmp[len] = '\0';

    int fork_child_pid = RedictModule_Fork(NULL, NULL);
    if (fork_child_pid < 0) {
        RedictModule_ReplyWithError(ctx, strerror(errno));
        return REDICTMODULE_OK;
    } else if (fork_child_pid > 0) {
        /* parent */
        RedictModule_ReplyWithSimpleString(ctx, "OK");
        return REDICTMODULE_OK;
    }

    RedictModuleRdbStream *stream = RedictModule_RdbStreamCreateFromFile(tmp);

    int ret = 0;
    if (RedictModule_RdbSave(ctx, stream, 0) != REDICTMODULE_OK) {
        ret = errno;
    }
    RedictModule_RdbStreamFree(stream);

    RedictModule_ExitFromChild(ret);
    return REDICTMODULE_OK;
}

int cmd_rdbload(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc != 2) {
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }

    size_t len;
    const char *filename = RedictModule_StringPtrLen(argv[1], &len);

    char tmp[len + 1];
    memcpy(tmp, filename, len);
    tmp[len] = '\0';

    RedictModuleRdbStream *stream = RedictModule_RdbStreamCreateFromFile(tmp);

    if (RedictModule_RdbLoad(ctx, stream, 0) != REDICTMODULE_OK || errno != 0) {
        RedictModule_RdbStreamFree(stream);
        RedictModule_ReplyWithError(ctx, strerror(errno));
        return REDICTMODULE_OK;
    }

    RedictModule_RdbStreamFree(stream);
    RedictModule_ReplyWithSimpleString(ctx, "OK");
    return REDICTMODULE_OK;
}

int RedictModule_OnLoad(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    if (RedictModule_Init(ctx, "rdbloadsave", 1, REDICTMODULE_APIVER_1) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx, "test.sanity", sanity, "", 0, 0, 0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx, "test.rdbsave", cmd_rdbsave, "", 0, 0, 0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx, "test.rdbsave_fork", cmd_rdbsave_fork, "", 0, 0, 0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx, "test.rdbload", cmd_rdbload, "", 0, 0, 0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    return REDICTMODULE_OK;
}
