// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-only

#include "redictmodule.h"

static void timer_callback(RedictModuleCtx *ctx, void *data)
{
    RedictModuleString *keyname = data;
    RedictModuleCallReply *reply;

    reply = RedictModule_Call(ctx, "INCR", "s", keyname);
    if (reply != NULL)
        RedictModule_FreeCallReply(reply);
    RedictModule_FreeString(ctx, keyname);
}

int test_createtimer(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    if (argc != 3) {
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }

    long long period;
    if (RedictModule_StringToLongLong(argv[1], &period) == REDICTMODULE_ERR) {
        RedictModule_ReplyWithError(ctx, "Invalid time specified.");
        return REDICTMODULE_OK;
    }

    RedictModuleString *keyname = argv[2];
    RedictModule_RetainString(ctx, keyname);

    RedictModuleTimerID id = RedictModule_CreateTimer(ctx, period, timer_callback, keyname);
    RedictModule_ReplyWithLongLong(ctx, id);

    return REDICTMODULE_OK;
}

int test_gettimer(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    if (argc != 2) {
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }

    long long id;
    if (RedictModule_StringToLongLong(argv[1], &id) == REDICTMODULE_ERR) {
        RedictModule_ReplyWithError(ctx, "Invalid id specified.");
        return REDICTMODULE_OK;
    }

    uint64_t remaining;
    RedictModuleString *keyname;
    if (RedictModule_GetTimerInfo(ctx, id, &remaining, (void **)&keyname) == REDICTMODULE_ERR) {
        RedictModule_ReplyWithNull(ctx);
    } else {
        RedictModule_ReplyWithArray(ctx, 2);
        RedictModule_ReplyWithString(ctx, keyname);
        RedictModule_ReplyWithLongLong(ctx, remaining);
    }

    return REDICTMODULE_OK;
}

int test_stoptimer(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    if (argc != 2) {
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }

    long long id;
    if (RedictModule_StringToLongLong(argv[1], &id) == REDICTMODULE_ERR) {
        RedictModule_ReplyWithError(ctx, "Invalid id specified.");
        return REDICTMODULE_OK;
    }

    int ret = 0;
    RedictModuleString *keyname;
    if (RedictModule_StopTimer(ctx, id, (void **) &keyname) == REDICTMODULE_OK) {
        RedictModule_FreeString(ctx, keyname);
        ret = 1;
    }

    RedictModule_ReplyWithLongLong(ctx, ret);
    return REDICTMODULE_OK;
}


int RedictModule_OnLoad(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);
    if (RedictModule_Init(ctx,"timer",1,REDICTMODULE_APIVER_1)== REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"test.createtimer", test_createtimer,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.gettimer", test_gettimer,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"test.stoptimer", test_stoptimer,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    return REDICTMODULE_OK;
}
