// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-only

#include "redictmodule.h"

#include <string.h>

void InfoFunc(RedictModuleInfoCtx *ctx, int for_crash_report) {
    RedictModule_InfoAddSection(ctx, "");
    RedictModule_InfoAddFieldLongLong(ctx, "global", -2);
    RedictModule_InfoAddFieldULongLong(ctx, "uglobal", (unsigned long long)-2);

    RedictModule_InfoAddSection(ctx, "Spanish");
    RedictModule_InfoAddFieldCString(ctx, "uno", "one");
    RedictModule_InfoAddFieldLongLong(ctx, "dos", 2);

    RedictModule_InfoAddSection(ctx, "Italian");
    RedictModule_InfoAddFieldLongLong(ctx, "due", 2);
    RedictModule_InfoAddFieldDouble(ctx, "tre", 3.3);

    RedictModule_InfoAddSection(ctx, "keyspace");
    RedictModule_InfoBeginDictField(ctx, "db0");
    RedictModule_InfoAddFieldLongLong(ctx, "keys", 3);
    RedictModule_InfoAddFieldLongLong(ctx, "expires", 1);
    RedictModule_InfoEndDictField(ctx);

    RedictModule_InfoAddSection(ctx, "unsafe");
    RedictModule_InfoBeginDictField(ctx, "unsafe:field");
    RedictModule_InfoAddFieldLongLong(ctx, "value", 1);
    RedictModule_InfoEndDictField(ctx);

    if (for_crash_report) {
        RedictModule_InfoAddSection(ctx, "Klingon");
        RedictModule_InfoAddFieldCString(ctx, "one", "wa'");
        RedictModule_InfoAddFieldCString(ctx, "two", "cha'");
        RedictModule_InfoAddFieldCString(ctx, "three", "wej");
    }

}

int info_get(RedictModuleCtx *ctx, RedictModuleString **argv, int argc, char field_type)
{
    if (argc != 3 && argc != 4) {
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }
    int err = REDICTMODULE_OK;
    const char *section, *field;
    section = RedictModule_StringPtrLen(argv[1], NULL);
    field = RedictModule_StringPtrLen(argv[2], NULL);
    RedictModuleServerInfoData *info = RedictModule_GetServerInfo(ctx, section);
    if (field_type=='i') {
        long long ll = RedictModule_ServerInfoGetFieldSigned(info, field, &err);
        if (err==REDICTMODULE_OK)
            RedictModule_ReplyWithLongLong(ctx, ll);
    } else if (field_type=='u') {
        unsigned long long ll = (unsigned long long)RedictModule_ServerInfoGetFieldUnsigned(info, field, &err);
        if (err==REDICTMODULE_OK)
            RedictModule_ReplyWithLongLong(ctx, ll);
    } else if (field_type=='d') {
        double d = RedictModule_ServerInfoGetFieldDouble(info, field, &err);
        if (err==REDICTMODULE_OK)
            RedictModule_ReplyWithDouble(ctx, d);
    } else if (field_type=='c') {
        const char *str = RedictModule_ServerInfoGetFieldC(info, field);
        if (str)
            RedictModule_ReplyWithCString(ctx, str);
    } else {
        RedictModuleString *str = RedictModule_ServerInfoGetField(ctx, info, field);
        if (str) {
            RedictModule_ReplyWithString(ctx, str);
            RedictModule_FreeString(ctx, str);
        } else
            err=REDICTMODULE_ERR;
    }
    if (err!=REDICTMODULE_OK)
        RedictModule_ReplyWithError(ctx, "not found");
    RedictModule_FreeServerInfo(ctx, info);
    return REDICTMODULE_OK;
}

int info_gets(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    return info_get(ctx, argv, argc, 's');
}

int info_getc(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    return info_get(ctx, argv, argc, 'c');
}

int info_geti(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    return info_get(ctx, argv, argc, 'i');
}

int info_getu(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    return info_get(ctx, argv, argc, 'u');
}

int info_getd(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    return info_get(ctx, argv, argc, 'd');
}

int RedictModule_OnLoad(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);
    if (RedictModule_Init(ctx,"infotest",1,REDICTMODULE_APIVER_1)
            == REDICTMODULE_ERR) return REDICTMODULE_ERR;

    if (RedictModule_RegisterInfoFunc(ctx, InfoFunc) == REDICTMODULE_ERR) return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"info.gets", info_gets,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"info.getc", info_getc,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"info.geti", info_geti,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"info.getu", info_getu,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"info.getd", info_getd,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    return REDICTMODULE_OK;
}
